﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    public class ChangeQuestion
    {
        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string question)
        {
            changeQuestion(groups, question);
        }

        public void changeQuestion(FormMain.GroupRec[] groups, string question)
        {
            if (string.IsNullOrEmpty(question))
            {
                formMain.getLog().AppendText("Question is empty." + "\n");
                return;
            }

            formMain.getLog().AppendText("New question: " + question + "\n");

            var driver = formMain.getDriver();

            for (int i = 0; i < groups.Length; i++)
            {
                var testGroup = groups[i].shortName;
                if (!testGroup.EndsWith("/"))
                {
                    testGroup = testGroup + "/";
                }
                if (!formMain.asyncSleep(5000)) { return; }

                if (!formMain.asyncSleep(0))
                {
                    formMain.getLog().AppendText("Stopped by user\n");
                    return;
                }

                formMain.getLog().AppendText("Proccessing group: " + testGroup + "\n");
                try
                {
                    driver.Navigate().GoToUrl(testGroup + "requests/");

                    if (!formMain.asyncSleep(5000)) { return; }
                    var settingsButton = driver.FindElement(By.XPath(".//a[contains(@ajaxify,'/groups/membership_criteria/edit/')]"));
                    settingsButton.Click();
                    if (!formMain.asyncSleep(5000)) { return; }
                    var questionFields = driver.FindElements(By.XPath(".//input[contains(@name,'criteria_questions[]')]"));
                    if (questionFields.Count == 0)
                    {
                        var questionDiv = driver.FindElement(By.XPath(".//div[contains(@class,'_2pin')]"));
                        var addQuestion = questionDiv.FindElement(By.XPath(".//i[contains(@class,'sp_')]"));
                        addQuestion.Click();
                        formMain.asyncSleep(5000);
                        questionFields = driver.FindElements(By.XPath(".//input[contains(@name,'criteria_questions[]')]"));
                    }
                    questionFields[0].Clear();
                    if (!formMain.asyncSleep(5000)) { return; }
                    questionFields[0].SendKeys(question);
                    if (!formMain.asyncSleep(5000)) { return; }
                    var confirmButton = driver.FindElement(By.ClassName("layerConfirm"));
                    confirmButton.Click();
                    if (!formMain.asyncSleep(1500)) { return; }

                    var okButton = driver.FindElement(By.ClassName("layerCancel"));
                    okButton.Click();
                }
                catch (Exception e)
                {
                    formMain.getLog().AppendText("Unable to set question to: " + groups[i].shortName + " Error: " + e.Message +"\n");
                }

                formMain.getLog().AppendText("Proccessing group: " + groups[i].shortName + " finished." + "\n");
            }
        }
    }
}
