﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFacebook
{
    static public class CommonUtils
    {
        public static bool isUrl(string s)
        {
            return s.StartsWith("http");
        }
    }
}
