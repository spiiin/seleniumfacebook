﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    public class PostToGroups
    {
        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string postText, string postAttachmentPath, int sleepBetweenPosts)
        {
            postToGroups(groups, postText, postAttachmentPath, sleepBetweenPosts);
        }

        private bool postToGroup(string testGroup, string postText, string postAttachmentPath)
        {
            if (!formMain.asyncSleep(3000)) { return false; }
            formMain.getLog().AppendText("Add post to group: " + testGroup + "\n");

            var driver = formMain.getDriver();
            driver.Navigate().GoToUrl(testGroup);

            if (!formMain.asyncSleep(5000)) { return false; }

            var textArea = driver.FindElement(By.XPath("//textarea[contains(@name,'xhpc_message_text')]"));
            textArea.SendKeys(postText);

            if (!formMain.asyncSleep(12000)) { return false; }
            //example format: "C:\test.png"

            var attachment = postAttachmentPath;
            if (attachment != "")
            {
                attachment = attachment.Replace("/", "\\");
                var addImage = driver.FindElement(By.XPath("//input[contains(@name,'composer_photo')]"));
                addImage.SendKeys(attachment);
                if (!formMain.asyncSleep(15000)) { return false; }
            }

            var postButton = driver.FindElement(By.XPath("//button[contains(@data-testid,'react-composer-post-button')]"));
            postButton.Click();
            if (!formMain.asyncSleep(5000)) { return false; }

            return true;
        }

        private void postToGroups(FormMain.GroupRec[] groups, string postText, string postAttachmentPath, int sleepBetweenPosts)
        {
            formMain.getLog().AppendText("Post to groups begin\n");
            for (int i = 0; i < groups.Length; i++)
            {
                if (!formMain.asyncSleep(0))
                {
                    formMain.getLog().AppendText("Stopped by user\n");
                    return;
                }
                var group = groups[i];
                formMain.getLog().AppendText(String.Format("Proccess group {0}/{1} - {2}\n", i + 1, groups.Length, group.fullName));
                try
                {
                    postToGroup(group.shortName, postText, postAttachmentPath);
                    if (!formMain.asyncSleep(sleepBetweenPosts * 1000)) { return; }
                }
                catch (Exception e)
                {
                    formMain.getLog().AppendText("Error while trying to add post to group " + groups[i] + " . Message:" + e.Message + "\n");
                }
            }
            formMain.getLog().AppendText("Adding posts complete!\n");
        }
    }
}
