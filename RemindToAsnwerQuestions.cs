﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    class RemindToAnswerQuestions
    {
        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, bool testMode, int pagesToScrollDown)
        {
            remindMembersToAnswer(groups, testMode, pagesToScrollDown);
        }
        private void remindMembersToAnswer(FormMain.GroupRec[] groups, bool testMode, int pagesToScrollDown)
        {
            formMain.getLog().AppendText("Remind to answer questions.\n");

            for (int i = 0; i < groups.Length; i++)
            {
                if (!formMain.asyncSleep(0))
                {
                    formMain.getLog().AppendText("Stopped by user\n");
                    return;
                }
                try
                {
                    var group = groups[i];
                    formMain.getLog().AppendText(String.Format("Proccess group {0}/{1} - {2}\n", i + 1, groups.Length, group.fullName));
                    remindMembersToAnswerForGroup(group.shortName, testMode, pagesToScrollDown);
                }
                catch (Exception e)
                {
                    formMain.getLog().AppendText("Error while trying to remind to answer questions for group " + groups[i] + " .Message:" + e.Message + "\n");
                }
            }
            formMain.getLog().AppendText("Remind to answer questions complete!\n");
        }

        private void remindMembersToAnswerForGroup(string testGroup, bool testMode, int pagesToScrollDown)
        {
            if (!formMain.asyncSleep(3000)) { return; }
            formMain.getLog().AppendText("Remind for group: " + testGroup + "\n");
            if (!testGroup.EndsWith("/"))
            {
                testGroup = testGroup + "/";
            }
            testGroup = testGroup + "requests";
            var driver = formMain.getDriver();
            driver.Navigate().GoToUrl(testGroup);

            if (!formMain.asyncSleep(6000)) { return; }

            //add pages
            int nPages = pagesToScrollDown;
            for (int i = 0; i < nPages; i++)
            {
                formMain.getLog().AppendText(String.Format("Scroll down page {0}/{1}\n", i + 1, nPages));
                JsHelpers.scrollToBottomPage(driver);
                JsHelpers.waitWhileDocumentLoaded(driver);
                if (!formMain.asyncSleep(5000)) { return; }
            }

            if (!formMain.asyncSleep(0))
            {
                formMain.getLog().AppendText("Stopped by user\n");
                return;
            }

            var aRequests = driver.FindElements(By.XPath(".//a")).Where(a => a.Text.Contains("Remind") && a.Text.Contains("to answer questions")).ToList();
            formMain.getLog().AppendText("Total " + aRequests.Count.ToString() + " requests\n");                       //print request count
            foreach (var request in aRequests)
            {
                formMain.getLog().AppendText("Remind to " + (request != null ? request.Text : "") + "\n");
                if (!testMode)
                {
                    request.Click();
                }
            }
        }
    }
}
