﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    class SearchMembers
    {
        public FormMain formMain { get; set; }

        public void doSearchMember(string testGroup, string fullName)
        {
            var driver = formMain.getDriver();
            driver.Navigate().GoToUrl(testGroup);

            formMain.getLog().AppendText("Search members\n");

            var searchInput = new WebDriverWait(formMain.getDriver(), new TimeSpan(0, 0, 10)).Until(ExpectedConditions.ElementExists(By.Name("member_query")));
            searchInput.Clear();
            searchInput.SendKeys(fullName);
            if (!formMain.asyncSleep(900)) { return; }
            searchInput.SendKeys(OpenQA.Selenium.Keys.Enter);
        }
    }
}
