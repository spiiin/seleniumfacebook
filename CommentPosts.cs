﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    public class CommentPosts
    {
        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string keyword, string comment, string tagInputTextBoxValue, int pagesToScrollDown)
        {
            commentPosts(groups, keyword, comment, tagInputTextBoxValue, pagesToScrollDown);
        }

        //TODO: Refactor tihs a little bit 
        private void commentPosts(FormMain.GroupRec[] groups, string keyword, string comment, string tagInputTextBoxValue, int pagesToScrollDown)
        {
            formMain.getLog().AppendText("Replay to post which contains:" + keyword + "\n");

            var group = tagInputTextBoxValue.Trim();
            var tag = "";

            if (group != String.Empty)
            {
                tag = getTag(group);
            }

            var driver = formMain.getDriver();
            for (int i = 0; i < groups.Length; i++)
            {
                if (!formMain.asyncSleep(0))
                {
                    formMain.getLog().AppendText("Stopped by user\n");
                    return;
                }
                try
                {
                    formMain.getLog().AppendText("Proccessing group:" + groups[i].shortName + "\n");

                    driver.Navigate().GoToUrl(groups[i].shortName);

                    for (int j = 0; j < pagesToScrollDown; j++)
                    {
                        formMain.getLog().AppendText(String.Format("Get posts from page {0}/{1}\n", j + 1, pagesToScrollDown));
                        if (!formMain.asyncSleep(5000)) { return; }
                        JsHelpers.scrollToBottomPage(driver);
                        JsHelpers.waitWhileDocumentLoaded(driver);
                    }

                    if (!formMain.asyncSleep(5000)) { return; }

                    var allPostsInGroup = driver.FindElements(By.XPath("//div[contains(@id,'mall_post_')]"));
                    formMain.getLog().AppendText("Total " + allPostsInGroup.Count.ToString() + " posts\n");

                    List<IWebElement> filteredPosts = new List<IWebElement>();

                    foreach (var post in allPostsInGroup)
                    {
                        try
                        {
                            try
                            {
                                var seeMoreButton = post.FindElement(By.ClassName("see_more_link"));
                                seeMoreButton.Click();
                            }
                            catch
                            { }

                            if (!formMain.asyncSleep(1500)) { return; }

                            var postValue = post.FindElement(By.ClassName("_5wj-"));

                            if (postValue != null && postValue.Text.Contains(keyword))
                                filteredPosts.Add(post);
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    formMain.getLog().AppendText("Number of post founded: " + filteredPosts?.Count + "\n");

                    //string tag = getTag();

                    if (tag != String.Empty)
                    {
                        foreach (var post in filteredPosts)
                            commentPostWithTag(post, comment, tag);
                    }
                    else
                    {
                        foreach (var post in filteredPosts)
                            commentPost(post, comment);

                        // Addtional process of last post
                        if (filteredPosts != null && filteredPosts.Count > 0)
                            commentPost(filteredPosts.Last(), comment);
                    }
                }
                catch (Exception e)
                {
                    formMain.getLog().AppendText("Error while trying to remove user from group " + groups[i] + " .Message:" + e.Message + "\n");
                }
            }

            formMain.getLog().AppendText("Replaying to posts complete!\n");
        }

        private void commentPost(IWebElement post, string comment)
        {
            try
            {
                //string postName = "UFIAddCommentInput";
                var postWriteCommentPart = post.FindElements(By.TagName("div")).Where(div => div.Text == ("Write a comment...")).FirstOrDefault();
                if (postWriteCommentPart == null)
                {
                    formMain.getLog().AppendText("Comment field was not found \n");
                    return;
                }
                postWriteCommentPart.SendKeys(comment);
                postWriteCommentPart.SendKeys(OpenQA.Selenium.Keys.Enter);
                var commentButton = post.FindElement(By.ClassName("_5yxe"));
                commentButton.Click();

                if (!formMain.asyncSleep(5000)) { return; }
            }
            catch (Exception ex)
            {
                formMain.getLog().AppendText(ex.ToString());
            }
        }

        private void commentPostWithTag(IWebElement post, string comment, string tag)
        {
            const int MAX_TAG_LENGTH = 22;
            try
            {
                var postWriteCommentPart = post.FindElements(By.TagName("div")).Where(div => div.Text == ("Write a comment...")).FirstOrDefault();
                if (postWriteCommentPart == null)
                {
                    formMain.getLog().AppendText("Comment field was not found \n");
                    return;
                }

                postWriteCommentPart.Click(); // Set focus to comment's input field
                postWriteCommentPart.SendKeys(comment + " ");  // Add comment
                postWriteCommentPart.SendKeys("@" + tag.Substring(0, MAX_TAG_LENGTH)); // Add tag
                if (!formMain.asyncSleep(3000)) { return; }

                var popupElems = formMain.getDriver().FindElements(By.ClassName("_5u8_"));
                if (popupElems != null && popupElems.Count > 0)
                {
                    var listItems = popupElems[0].FindElements(By.ClassName("_5u94"));
                    foreach (var li in listItems)
                        if (li.Text == tag)
                        {
                            li.Click();
                            break;
                        }
                }
                else
                {
                    postWriteCommentPart.SendKeys(OpenQA.Selenium.Keys.Enter); // Accept popup
                    if (!formMain.asyncSleep(1000)) { return; }
                }

                postWriteCommentPart.SendKeys(OpenQA.Selenium.Keys.Enter); // Accept comment
                if (!formMain.asyncSleep(1000)) { return; }
            }
            catch (Exception ex)
            {
                formMain.getLog().AppendText(ex.ToString());
            }
        }

        private string getTag(string groupNameOrLink)
        {
            string tag = "";
            if (CommonUtils.isUrl(groupNameOrLink))
            {
                string groupUrl = groupNameOrLink;
                tag = formMain.parseGroupName(groupUrl);
            }
            else
            {
                tag = groupNameOrLink;
            }

            return tag;
        }
    }
}
