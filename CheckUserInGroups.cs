﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    public class CheckIfUserInGroups
    {
        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string[] urlsToCheck)
        {
            checkIfUserInGroups(groups, urlsToCheck);
        }

        private void checkIfUserInGroups(FormMain.GroupRec[] groups, string[] urlsToCheck)
        {
            int urlsCount = urlsToCheck.Length;
            for (int u = 0; u < urlsCount; u++)
            {
                var linkToRemove = urlsToCheck[u];
                var foundInGroups = new List<string>();
                var notFoundInGroups = new List<string>();
                formMain.getLog().AppendText("Searching in groups begin. User: " + linkToRemove + "\n");
                formMain.getLog().AppendText("Link to search: " + linkToRemove + "\n");
                var fullnameToRemove = linkToRemove;
                if (CommonUtils.isUrl(linkToRemove))
                {
                    var newLinkToRemove = linkToRemove;
                    fullnameToRemove = new FindFullName() { formMain = this.formMain }.findFullName(linkToRemove, out newLinkToRemove);
                }
                formMain.getLog().AppendText("Name to search: " + fullnameToRemove + "\n");

                for (int i = 0; i < groups.Length; i++)
                {
                    if (!formMain.asyncSleep(0))
                    {
                        formMain.getLog().AppendText("Stopped by user\n");
                        return;
                    }
                    try
                    {
                        var group = groups[i];
                        formMain.getLog().AppendText(String.Format("Proccess group {0}/{1} - {2}\n", i + 1, groups.Length, group.fullName));
                        if (checkIfUserInGroup(group.shortName, linkToRemove, fullnameToRemove))
                        {
                            foundInGroups.Add(group.fullName);
                        }
                        else
                        {
                            notFoundInGroups.Add(group.fullName);
                        }
                    }
                    catch (Exception e)
                    {
                        formMain.getLog().AppendText("Error while trying to remove user from group " + groups[i] + " .Message:" + e.Message + "\n");
                    }
                }
                formMain.getLog().AppendText("Searching in groups complete!\n");
                //print results
                formMain.getLog().AppendText("\n");
                formMain.getLog().AppendText(String.Format("User {0} found in groups:\n", linkToRemove));
                foreach (var g in foundInGroups)
                {
                    formMain.getLog().AppendText(g + "\n");
                }

                formMain.getLog().AppendText(String.Format("User {0} not found in groups:\n", linkToRemove));
                foreach (var g in notFoundInGroups)
                {
                    formMain.getLog().AppendText(g + "\n");
                }
                formMain.getLog().AppendText("\n");
            }
        }

        private bool checkIfUserInGroup(string testGroup, string linkToRemove, string fullnameToRemove)
        {
            if (!formMain.asyncSleep(3000)) { return false; }
            formMain.getLog().AppendText("Searching in group: " + testGroup + "\n");
            if (!testGroup.EndsWith("/"))
            {
                testGroup = testGroup + "/";
            }
            testGroup = testGroup + "members";

            new SearchMembers() { formMain = this.formMain }.doSearchMember(testGroup, fullnameToRemove);

            if (!formMain.asyncSleep(6000)) { return false; }

            var memberDivs = formMain.getDriver().FindElements(By.XPath("//div[@class='_6a _5u5j _6b']"));
            if (memberDivs.Count == 0)
            {
                formMain.getLog().AppendText("User " + linkToRemove + " not found in group " + testGroup + " !\n");
                return false;
            }
            foreach (var memberDiv in memberDivs)
            {
                var linkDiv = memberDiv.FindElement(By.XPath(".//div[contains(@class, 'fsl fwb fcb')]"));
                var link = linkDiv.FindElement(By.TagName("a"));
                var linkHref = link.GetAttribute("href");
                formMain.getLog().AppendText(linkHref + "\n");
                bool linkIsUrl = CommonUtils.isUrl(linkToRemove);
                if (!linkIsUrl || linkHref.Contains(linkToRemove))
                {
                    formMain.getLog().AppendText("Member found!\n");
                    return true;
                }
            }
            return false;
        }
    }
}
