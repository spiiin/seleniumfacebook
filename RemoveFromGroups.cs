﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    class RemoveFromGroups
    {

        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string[] urlsToBlock, bool blockUser, bool testMode, bool deletePosts7, bool deleteComments7, bool deleteInvites7)
        {
            removeFromGroups(groups, urlsToBlock, blockUser, testMode, deletePosts7, deleteComments7, deleteInvites7);
        }

        private void removeFromGroups(FormMain.GroupRec[] groups, string[] urlsToBlock, bool blockUser, bool testMode, bool deletePosts7, bool deleteComments7, bool deleteInvites7)
        {
            formMain.appendUrlListToTxtFile();
            int urlsCount = urlsToBlock.Length;
            for (int u = 0; u < urlsCount; u++)
            {
                var linkToRemove = urlsToBlock[u];
                formMain.getLog().AppendText("Removing from groups begin. User: " + linkToRemove + "\n");
                formMain.getLog().AppendText("Link to remove: " + linkToRemove + "\n");
                var fullnameToRemove = linkToRemove;
                if (CommonUtils.isUrl(linkToRemove))
                {
                    var newLinkToRemove = linkToRemove;
                    fullnameToRemove = new FindFullName() { formMain = this.formMain }.findFullName(linkToRemove, out newLinkToRemove);
                    linkToRemove = newLinkToRemove;
                }
                formMain.getLog().AppendText("Name to remove: " + fullnameToRemove + "\n");

                for (int i = 0; i < groups.Length; i++)
                {
                    if (!formMain.asyncSleep(0))
                    {
                        formMain.getLog().AppendText("Stopped by user\n");
                        return;
                    }
                    try
                    {
                        var group = groups[i];
                        formMain.getLog().AppendText(String.Format("Proccess group {0}/{1} - {2}\n", i + 1, groups.Length, group.fullName));
                        removeFromGroup(group.shortName, linkToRemove, fullnameToRemove, blockUser, testMode, deletePosts7, deleteComments7, deleteInvites7);
                    }
                    catch (Exception e)
                    {
                        formMain.getLog().AppendText("Error while trying to remove user from group " + groups[i] + " .Message:" + e.Message + "\n");
                    }
                }
                formMain.getLog().AppendText("Removing from groups complete!\n");
            }
        }

        private void removeFromGroup(string testGroup, string linkToRemove, string fullnameToRemove, bool blockUser, bool testMode, bool deletePosts7, bool deleteComments7, bool deleteInvites7)
        {
            if (!formMain.asyncSleep(3000)) { return; }
            formMain.getLog().AppendText("Removing from group: " + testGroup + "\n");
            if (!testGroup.EndsWith("/"))
            {
                testGroup = testGroup + "/";
            }
            testGroup = testGroup + "members";

            new SearchMembers() { formMain = this.formMain }.doSearchMember(testGroup, fullnameToRemove);

            if (!formMain.asyncSleep(6000)) { return; }

            var memberDivs = formMain.getDriver().FindElements(By.XPath("//div[@class='_6a _5u5j _6b']"));
            if (memberDivs.Count == 0)
            {
                formMain.getLog().AppendText("User " + linkToRemove + " not found in group " + testGroup + " !\n");
                return;
            }
            foreach (var memberDiv in memberDivs)
            {
                bool linkIsUrl = CommonUtils.isUrl(linkToRemove);
                if (linkIsUrl)
                {
                    var linkDiv = memberDiv.FindElement(By.XPath(".//div[contains(@class, 'fsl fwb fcb')]"));
                    var link = linkDiv.FindElement(By.TagName("a"));
                    var linkHref = link.GetAttribute("href");
                    //tbLog.AppendText(linkHref + "\n");
                    if (!linkHref.Contains(linkToRemove))
                    {
                        continue; //found member with same name, not remove it.
                    }
                }
                formMain.getLog().AppendText("Member found! Trying to remove...\n");
                clickToAdminButton(memberDiv);
                clickToRemoveLink();
                formMain.getLog().AppendText("Click remove button\n");
                if (deletePosts7)
                {
                    clickToDeletePosts();
                }
                if (deleteComments7)
                {
                    clickToDeleteComments();
                }
                if (deleteInvites7)
                {
                    clickToDeleteInvites();
                }
                if (blockUser)
                {
                    clickToBlockUser();
                }
                if (!testMode)
                {
                    clickToConfirmButton();
                }

                bool extendedRemove = deleteComments7 || deleteInvites7 || deletePosts7;
                if (extendedRemove)
                {
                    if (!formMain.asyncSleep(10000)) { return; } //wait while facebook remove all posts, comments and invites from removed person
                }

                formMain.getLog().AppendText("User " + linkToRemove + " removed from group " + testGroup + " !\n");
                break;
            }
        }

        private bool clickToAdminButton(IWebElement memberDiv)
        {
            var adminButton = memberDiv.FindElement(By.TagName("button"));
            adminButton.Click();
            return true;
        }

        private bool clickToRemoveLink()
        {
            if (!formMain.asyncSleep(500)) { return false; }
            var adminLinks = formMain.getDriver().FindElements(By.XPath("//a[@class='_54nc']"));
            foreach (var adminLink in adminLinks)
            {
                var linkHref = adminLink.GetAttribute("href");
                if (linkHref.Contains("remove.php"))
                {
                    adminLink.Click();
                    return true;
                }
            }
            return false;
        }

        private bool clickToDeletePosts()
        {
            if (!formMain.asyncSleep(1000)) { return false; }
            var by = By.XPath("//input[contains(@name,'delete_posts')]");
            var banUser = formMain.getDriver().FindElements(by);
            if (banUser.Count > 0)
            {
                banUser[0].Click();
                return true;
            }
            return false;
        }

        private bool clickToDeleteComments()
        {
            if (!formMain.asyncSleep(100)) { return false; }
            var by = By.XPath("//input[contains(@name,'delete_comments')]");
            var banUser = formMain.getDriver().FindElements(by);
            if (banUser.Count > 0)
            {
                banUser[0].Click();
                return true;
            }
            return false;
        }

        private bool clickToDeleteInvites()
        {
            if (!formMain.asyncSleep(100)) { return false; }
            var by = By.XPath("//input[contains(@name,'delete_invites')]");
            var banUser = formMain.getDriver().FindElements(by);
            if (banUser.Count > 0)
            {
                banUser[0].Click();
                return true;
            }
            return false;
        }

        private bool clickToBlockUser()
        {
            if (!formMain.asyncSleep(1000)) { return false; }
            var banUser = formMain.getDriver().FindElement(By.XPath("//input[contains(@name,'ban_user')]"));
            banUser.Click();
            return true;
        }

        private bool clickToConfirmButton()
        {
            if (!formMain.asyncSleep(2000)) { return false; }
            var confirmButton = formMain.getDriver().FindElement(By.XPath("//button[contains(@class,'layerConfirm')]"));
            confirmButton.Click();
            return true;
        }
    }
}
