﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    public class PostJumper
    {
        public FormMain formMain { get; set; }

        public void start(string[] posts, string comment)
        {
            commentPosts(posts, comment);
        }

        private void commentPosts(string[] posts, string comment)
        {
            formMain.getLog().AppendText("Jumper start\n");
            var driver = formMain.getDriver();

            for (int i = 0; i < posts.Length; i++)
            {
                if (!formMain.asyncSleep(0))
                {
                    formMain.getLog().AppendText("Stopped by user\n");
                    return;
                }
                var post = posts[i].Split(' ')[0];
                formMain.getLog().AppendText(String.Format("Proccessing post ({0}/{1}): {2}\n", i+1, posts.Length, posts[i]));
                try
                {
                    commentPost(post, comment);
                    formMain.asyncSleep(10000);
                    deleteCommentFromPost(post, comment);
                }
                catch (Exception e)
                {
                    formMain.getLog().AppendText("Error while comment post" + post + " . Message:" + e.Message + "\n");
                }
            }

            formMain.getLog().AppendText("Replaying to posts complete!\n");
        }

        private void commentPost(string post, string comment)
        {
            var driver = formMain.getDriver();
            try
            {
                driver.Navigate().GoToUrl(post);
                if (!formMain.asyncSleep(3000)) { return; }

                var commentDiv = driver.FindElement(By.XPath("//div[contains(@class,'UFIAddCommentInput')]"));
                commentDiv.Click();

                //after click on comment dom changed andd new divs shows
                if (!formMain.asyncSleep(3000)) { return; }
                var postWriteCommentPart = driver.FindElement(By.XPath("//div[contains(@class,'_1mf _1mj')]"));
                postWriteCommentPart.SendKeys(comment);
                postWriteCommentPart.SendKeys(OpenQA.Selenium.Keys.Enter);

                if (!formMain.asyncSleep(5000)) { return; }
            }
            catch (Exception ex)
            {
                formMain.getLog().AppendText("Error while commenting post " + post + ": " + ex.ToString() + "\n");
            }
        }

        private void deleteCommentFromPost(string post, string comment)
        {
            var driver = formMain.getDriver();
            var commentBlocks = driver.FindElements(By.XPath(".//div[contains(@class,'UFICommentContentBlock')]"));
            if (commentBlocks.Count > 0)
            {
                formMain.getLog().AppendText("Found total comments: " + commentBlocks.Count + "\n");
            }
            foreach (var commentBlock in commentBlocks)
            {
                var commentBodyParent = commentBlock.FindElement(By.XPath(".//span[contains(@class,'UFICommentBody')]"));
                var commentText = commentBodyParent.Text;
                //formMain.getLog().AppendText("Comment text: " + commentText + "\n");
                bool needRemove = commentText.Contains(comment);
                if (needRemove)
                {
                    formMain.getLog().AppendText("Found jumper comment Trying to remove...\n");
                    clickToCommentAdminButton(commentBlock);
                    clickToCommentRemoveButton();
                    clickToRemoveCommentConfirmButton();
                    formMain.getLog().AppendText("Comment removed!\n");
                    return;
                }
            }
        }

        //--------------copy-paste from RemovePostsAndComments, need to remove-------------
        private bool clickToCommentAdminButton(IWebElement commentBlock)
        {
            var adminButton = commentBlock.FindElement(By.XPath(".//a[contains(@class,'UFICommentCloseButton')]"));
            adminButton.Click();
            return true;
        }

        private bool clickToCommentRemoveButton()
        {
            if (!formMain.asyncSleep(3000)) { return false; }
            var adminLinks = formMain.getDriver().FindElements(By.XPath("//a[contains(@class,'_54nc')]"));
            foreach (var adminLink in adminLinks)
            {
                var attr = adminLink.GetAttribute("data-testid");
                if (attr.Contains("ufi_comment_menu_delete"))
                {
                    adminLink.Click();
                    return true;
                }
            }
            return false;
        }

        private bool clickToRemoveCommentConfirmButton()
        {
            if (!formMain.asyncSleep(3000)) { return false; }
            var confirmButton = formMain.getDriver().FindElement(By.XPath("//a[contains(@data-testid,'ufi_hide_dialog_delete_button')]"));
            confirmButton.Click();
            return true;
        }
        //--------------copy-paste-------------
    }
}
