﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    public partial class FormMain : Form
    {
#if kill
        FirefoxDriver driver;
#else // kill
        IWebDriver driver;
#endif // kill
        bool gStop = false;

        public bool asyncSleep(int msec)
        {
            if (msec < 1)
            {
                return !gStop;
            }

            DateTime _desired = DateTime.Now.AddMilliseconds(msec);
            while (DateTime.Now < _desired)
            {
                Thread.Sleep(1);
                System.Windows.Forms.Application.DoEvents();
                if (gStop)
                {
                    return false;
                }
            }
            return !gStop;
        }

        public TextBox getLog()
        {
            return tbLog;
        }

        public IWebDriver getDriver()
        {
            return driver;
        }

        public void initSelenium()
        {
#if !kill
            driver = new FirefoxDriver();
#else // kill
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
#endif // kill
        }

        private void loginToFacebook()
        {
            tbLog.Clear();
            tbLog.AppendText("Login to facebook...\n");

            var lines = System.IO.File.ReadAllLines(tbCredentialsSimple.Text);
            var login = lines[0];
            var pass = lines[1];
            driver.Navigate().GoToUrl("http://facebook.com");
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            var email = wait.Until(ExpectedConditions.ElementExists(By.Name("email")));
            email.Clear();
            email.SendKeys(login);
            var password = driver.FindElement(By.Name("pass"));
            password.Clear();
            password.SendKeys(pass);
            var loginButton = driver.FindElement(By.Id("loginbutton"));
            if (!asyncSleep(2000)) { return; }
            loginButton.Click();

            tbLog.AppendText("Login to facebook complete\n");
            asyncSleep(3000);
        }

        public string parseGroupName(string url)
        {
            string tag = "";
            string prevUrl = driver.Url;
            driver.Navigate().GoToUrl(url);

#if kill
            string groupName = driver.FindElementByXPath("//*[@id='seo_h1_tag']/a").Text;
#else // kill
            string groupName = driver.FindElement(By.XPath("//*[@id='seo_h1_tag']/a")).Text;
#endif // kill
            tag = groupName;

            driver.Navigate().GoToUrl(prevUrl);
            return tag;
        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            initSelenium();

            //auto login
            loginToFacebook();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            DoResize();

            for (int i = 0; i < 100; i++)
            {
                cbPagesToScrollDown.Items.Add(i.ToString());
            }
            cbPagesToScrollDown.SelectedIndex = 3; //scroll down 3 pages by default

#if !kill
            UpdateGroupsListview(ContextEnum_E.Simple);
            UpdateGroupsListview(ContextEnum_E.Full);
#endif // !kill
        }

        public class GroupRec
        {
            public string shortName;
            public string fullName;
#if !kill
            public string url
            {
                get { return shortName; }
            }

            public string friendlyName
            {
                get
                {
                    if (fullName.Contains(" "))
                        return string.Join(" ", fullName.Split(' ').Skip(1));
                    return "";
                }
            }

            public override string ToString()
            {
                return friendlyName;
            }
#endif // !kill
        }

        private GroupRec[] getSelectedGroups(ContextEnum_E context)
        {
            CheckedListBox lb = context == ContextEnum_E.Simple ? checkedListBoxGroupsSimple : checkedListBoxGroupsFull;
            List<GroupRec> ret = new List<GroupRec>();
#if kill
            foreach (ListViewItem itm in listViewGroups.Items)
            {
                if (itm.Checked)
                    ret.Add((GroupRec)itm.Tag);
            }
#else // kill
            foreach (var itm in lb.CheckedItems)
                ret.Add(itm as GroupRec);
#endif // kill
            return ret.ToArray();
        }

        private GroupRec[] parseGroupFile(ContextEnum_E context)
        {
            if (this.oneGroupOnly.Checked)
            {
                var oneItemOnly = new GroupRec[1];
                oneItemOnly[0].fullName = oneGroupTextBox.Text;
                oneItemOnly[0].shortName = oneGroupTextBox.Text.Split(' ')[0];
                return oneItemOnly;
            }

            TextBox tb = context == ContextEnum_E.Simple ? tbUrlToGroupSimple : tbUrlToGroupFull;
            var ans = new List<GroupRec>();
            if (System.IO.File.Exists(tb.Text))
            {
                var lines = System.IO.File.ReadAllLines(tb.Text);
                for (int i = 0; i < lines.Length; i++)
                {
#if kill
                if (lines[i].StartsWith("#")) //symbol for skip groups
                {
                    continue;
                }
#endif // kill
                    var curGroup = new GroupRec();
                    curGroup.fullName = lines[i];
                    curGroup.shortName = lines[i].Split(' ')[0];
                    ans.Add(curGroup);
                }
            }
            return ans.ToArray();
        }

        public void appendUrlListToTxtFile()
        {
            var profilesToBlockFile = "BlockedProfiles.txt";
            var lines = System.IO.File.Exists(profilesToBlockFile) ? System.IO.File.ReadAllLines(profilesToBlockFile) : new string[0];
            var profilesToAppend = tbUrlToBlock.Lines;
            using (System.IO.StreamWriter w = System.IO.File.AppendText(profilesToBlockFile))
            {
                foreach (var profileToAppend in profilesToAppend)
                {
                    if (!lines.Any(x => x == profileToAppend))
                    {
                        w.WriteLine(profileToAppend);
                    }
                }
            }
        }

        private enum ContextEnum_E { Simple, Full }
#if !kill
        private void UpdateGroupsListview(ContextEnum_E context)
        {
            CheckedListBox lb = context == ContextEnum_E.Simple ? checkedListBoxGroupsSimple : checkedListBoxGroupsFull;

            var groups = parseGroupFile(context);
            lb.Items.Clear();
            int column_width = 0;
            using (Graphics g = lb.CreateGraphics())
            {
                foreach (var group in groups)
                {
                    lb.Items.Add(group, false);
                    int this_width = (int)Math.Ceiling(g.MeasureString(group.ToString(), lb.Font).Width);
                    if (this_width > column_width)
                        column_width = this_width;
                }
            }
            lb.ColumnWidth = column_width + 30;


        }
#endif // !kill

        private void tbUrlToGroup_Click(object sender, EventArgs e)
        {
            if (ofSelectFile.ShowDialog() == DialogResult.OK)
            {
                TextBox tb = sender as TextBox;
                tb.Text = ofSelectFile.FileName;

#if !kill
                UpdateGroupsListview(tb == tbUrlToGroupSimple ? ContextEnum_E.Simple : ContextEnum_E.Full);
#endif // !kill
            }
        }

        private void tbCredentials_Click(object sender, EventArgs e)
        {
            if (ofSelectCredentials.ShowDialog() == DialogResult.OK)
            {
                TextBox tb = sender as TextBox;
                tb.Text = ofSelectCredentials.FileName;
            }
        }

        private void tbPostAttachment_Click(object sender, EventArgs e)
        {
            if (ofSelectAttachment.ShowDialog() == DialogResult.OK)
            {
                TextBox tb = sender as TextBox;
                tb.Text = ofSelectAttachment.FileName;
            }
        }

        private void tbJumperPosts_Click(object sender, EventArgs e)
        {
            if (ofSelectJumperPosts.ShowDialog() == DialogResult.OK)
            {
                TextBox tb = sender as TextBox;
                tb.Text = ofSelectJumperPosts.FileName;
            }
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (driver != null)
                driver.Quit();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            gStop = true;
        }

        private void btClearLog_Click(object sender, EventArgs e)
        {
            tbLog.Clear();
        }

        private void cbRun_Click(object sender, EventArgs e)
        {
            gStop = false;
            var groups = getSelectedGroups(ContextEnum_E.Full);
            if (cbRemoveFromGroups.Checked)
            {
                new RemoveFromGroups() { formMain = this }.start(groups, tbUrlToBlock.Lines, cbBlockUser.Checked, cbTestMode.Checked, cbDeletePosts7.Checked, cbDeleteComments7.Checked, cbDeleteInvites7.Checked);
            }
            if (cbRemoveMemberRequests.Checked)
            {
                new RemoveMemberRequests() { formMain = this }.start(groups, tbUrlToBlock.Lines, cbBlockUser.Checked, cbTestMode.Checked);
            }
            if (cbRemovePostsAndComments.Checked)
            {
                new RemovePostsAndComments() { formMain = this }.start(groups, tbUrlToBlock.Lines, cbTestMode.Checked, Int32.Parse(cbPagesToScrollDown.Text));
            }
            if (cbUserInGroups.Checked)
            {
                new CheckIfUserInGroups() { formMain = this }.start(groups, tbUrlToBlock.Lines);
            }
            if (cbRemingMembers.Checked)
            {
                new RemindToAnswerQuestions() { formMain = this }.start(groups, cbTestMode.Checked, cbPagesToScrollDown.SelectedIndex);
            }
        }

        private void submitCommentButton_Click(object sender, EventArgs e)
        {
            gStop = false;
            submitCommentButton.Enabled = false;

            try
            {
                List<string> words = new List<string>(), comments = new List<string>();

                if (cbWordsAndCommentsFile.Checked)
                {
                    string file_path = tbWordsAndComments.Text;
                    if (!System.IO.File.Exists(file_path))
                    {
                        MessageBox.Show("the file doesnt exist:\n" + file_path);
                        return;
                    }

                    string[] lines_in_file = System.IO.File.ReadAllLines(file_path).ToArray();
                    foreach (var line in lines_in_file)
                    {
                        if (line == "")
                            continue;

                        int pos = line.IndexOf('\t');
                        if (pos < 0)
                        {
                            MessageBox.Show("The following line in " + System.IO.Path.GetFileName(file_path) + " will be ignored because <tab> is not found.\n" + line);
                            continue;
                        }

                        string word = line.Substring(0, pos).Trim();
                        string comment = line.Substring(pos).TrimStart('\t').Trim();

                        words.Add(word);
                        comments.Add(comment);
                    }
                }

                if (cbManualInputWordAndComment.Checked)
                {
                    if (tbWord.Text == "" || tbComment.Text == "")
                    {
                        if (MessageBox.Show("Words or comments is empty, are you sure to continue?", "Selenium Facebook", MessageBoxButtons.YesNo) == DialogResult.No)
                            return;
                    }

                    words.Add(tbWord.Text);
                    comments.Add(tbComment.Text);
                }

                if (words.Count == 0)
                {
                    if (cbWordsAndCommentsFile.Checked)
                        MessageBox.Show("Nothing to do because no usable word/comment can be found from " + System.IO.Path.GetFileName(tbWordsAndComments.Text));
                    return;
                }

                string tag = tagInputTextBox.Text;
                int scroll = Int32.Parse(cbPagesToScrollDown.Text);

                for (int i = 0; i < words.Count; i++)
                {
                    string word = words[i];
                    string comment = comments[i];

                    Application.DoEvents();

                    new CommentPosts() { formMain = this }.start(getSelectedGroups(ContextEnum_E.Full), word, comment, tag, scroll);
                }
            }
            finally
            {
                submitCommentButton.Enabled = true;
            }
        }

        private void submitCommentQuestion_Click(object sender, EventArgs e)
        {
            gStop = false;
            new ChangeQuestion() { formMain = this }.start(getSelectedGroups(ContextEnum_E.Full), this.newQuestionTextBox.Text);
        }

        private void postButton_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            ContextEnum_E context;
            TextBox post_text_tb, post_attachment_tb, post_sleep_tb;
            GroupRec[] groups;
            if (btn == postButtonSimple)
            {
                context = ContextEnum_E.Simple;
                post_text_tb = tbPostTextSimple;
                post_attachment_tb = tbPostAttachmentSimple;
                post_sleep_tb = tbPostSleepSimple;
            }
            else
            {
                context = ContextEnum_E.Full;
                post_text_tb = tbPostTextFull;
                post_attachment_tb = tbPostAttachmentFull;
                post_sleep_tb = tbPostSleepFull;
            }

            groups = getSelectedGroups(context);

            btn.Enabled = false;
            gStop = false;
            new PostToGroups() { formMain = this }.start(groups, string.Join("\n", post_text_tb.Lines), post_attachment_tb.Text, Int32.Parse(post_sleep_tb.Text));
            btn.Enabled = true;
        }

        private void btPostJumpers_Click(object sender, EventArgs e)
        {
            gStop = false;
            var posts = System.IO.File.ReadAllLines(tbJumperPostsSimple.Text);
            new PostJumper() { formMain = this }.start(posts, tbJumperComment.Text);
        }

        private void oneGroupOnly_CheckedChanged(object sender, EventArgs e)
        {
#if !kill
            UpdateGroupsListview(ContextEnum_E.Full);
#endif // !kill
        }

        private void oneGroupTextBox_TextChanged(object sender, EventArgs e)
        {
#if !kill
            UpdateGroupsListview(ContextEnum_E.Full);
#endif // !kill
        }

        private DateTime lastGroupFileChange = DateTime.MinValue;
        private void checkGroupFileTimer_Tick(object sender, EventArgs e)
        {
#if future
            if (!postButton.Enabled)
                return;

            List<GroupRec> all_groups = new List<GroupRec>();
            all_groups.AddRange(parseGroupFile());

            for (int i = listViewGroups.Items.Count - 1; i >= 0; i--)
            {
                ListViewItem itm = listViewGroups.Items[i];
                var group = (GroupRec)itm.Tag;

                int idx = all_groups.FindIndex(g => { return g.url == group.url; });
                if (idx != -1)
                    all_groups.RemoveAt(idx);
                else
                    listViewGroups.Items.RemoveAt(i);
            }

            foreach (var group in all_groups)
            {
                var itm = listViewGroups.Items.Add(new ListViewItem(new string[] { "", group.friendlyName }));
                itm.Tag = group;
                itm.Checked = true;
            }
#endif // future
        }

        private void linkLabelReload_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
#if !kill
            LinkLabel lb = sender as LinkLabel;
            UpdateGroupsListview(lb == linkLabelReloadSimple ? ContextEnum_E.Simple : ContextEnum_E.Full);
            MessageBox.Show("Reloaded successfully");
#endif // !kill
        }

        private void DoResize()
        {
            panelFull.Left = panelFull.Top = 0;
            panelFull.Width = DisplayRectangle.Width;
            panelFull.Height = DisplayRectangle.Height;
            DoResizeFull();

            panelSimple.Left = panelSimple.Top = 0;
            panelSimple.Width = DisplayRectangle.Width;
            panelSimple.Height = DisplayRectangle.Height;
            DoResizeSimple();
        }

        private void DoResizeFull()
        {
            checkedListBoxGroupsFull.Width = panelFull.DisplayRectangle.Width - checkedListBoxGroupsFull.Left - 5;
            checkedListBoxGroupsFull.Height = panelFull.DisplayRectangle.Height - checkedListBoxGroupsFull.Top - 5;
            linkLabelReloadFull.Left = checkedListBoxGroupsFull.Right - linkLabelReloadFull.Width;
            checkBoxGroupFull.Left = linkLabelReloadFull.Left - checkBoxGroupFull.Width - 5;
        }

        private void DoResizeSimple()
        {

#if !kill
            Control[] column1 = new Control[]
            {
                lbCredentials, lbUrlToGroup, lbGroupsToManipulate, lbPostText, lbPostFileAttachment, lbSleepBetweenPosts, lbFileWithPostsUrls
            };

            Control[] column2 = new Control[]
            {
                tbCredentialsSimple, tbUrlToGroupSimple, checkedListBoxGroupsSimple, tbPostTextSimple, tbPostAttachmentSimple, tbPostSleepSimple, tbJumperPostsSimple
            };

            int margin = 15;
            int padding = 5;

            int total_height_except_listboxgroups = margin;
            foreach (var c2 in column2)
            {
                if (c2 != checkedListBoxGroupsSimple)
                    total_height_except_listboxgroups += c2.Height + padding;
            }
            total_height_except_listboxgroups += postButtonSimple.Height + margin;

            checkedListBoxGroupsSimple.Height = panelSimple.DisplayRectangle.Height - total_height_except_listboxgroups - padding;

            int column1_width = int.MinValue;
            foreach (var c in column1)
            {
                if (column1_width < c.Width)
                    column1_width = c.Width;
            }

            int top = margin;
            int left = margin;

            Func<Control, Control, bool> append = (c1, c2) =>
            {
                c1.Left = left;
                c1.Top = top;

                c2.Left = left + column1_width + padding;
                c2.Top = top;
                c2.Width = Math.Min(c2 == checkedListBoxGroupsSimple ? int.MaxValue : 300, panelSimple.DisplayRectangle.Width - margin - c2.Left);

                top = Math.Max(c1.Bottom, c2.Bottom) + padding;
                return true;
            };

            for (int row = 0; row < column1.Length; row++)
                append(column1[row], column2[row]);

            linkLabelReloadSimple.Top = lbGroupsToManipulate.Bottom + padding;
            linkLabelReloadSimple.Left = checkedListBoxGroupsSimple.Left - padding - linkLabelReloadSimple.Width;
            checkBoxGroupSimple.Top = linkLabelReloadSimple.Bottom + padding;
            checkBoxGroupSimple.Left = checkedListBoxGroupsSimple.Left - padding - checkBoxGroupSimple.Width;

            postButtonSimple.Left = left + column1_width + padding;
            postButtonSimple.Top = top;
#endif // !kill
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            DoResize();
        }

        bool checking = false;
        private void checkBoxGroup_CheckedChanged(object sender, EventArgs e)
        {
            checking = true;
            CheckBox cb = sender as CheckBox;
            CheckedListBox lb = cb == checkBoxGroupSimple ? checkedListBoxGroupsSimple : checkedListBoxGroupsFull;

            for (int i = 0; i < lb.Items.Count; i++)
                lb.SetItemChecked(i, cb.CheckState == CheckState.Checked);
            checking = false;
        }

        private void checkedListBoxGroup_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        {
            if (checking)
                return;

            CheckedListBox lb = sender as CheckedListBox;
            CheckBox cb = lb == checkedListBoxGroupsSimple ? checkBoxGroupSimple : checkBoxGroupFull;

            if (lb.CheckedItems.Count == lb.Items.Count - 1 && e.NewValue == CheckState.Checked)
                cb.CheckState = CheckState.Checked;
            else if (lb.CheckedItems.Count == 1 && e.NewValue == CheckState.Unchecked)
                cb.CheckState = CheckState.Unchecked;
            else
                cb.CheckState = CheckState.Indeterminate;
        }

        private void tbWordsAndComments_Click(object sender, EventArgs e)
        {
            if (ofSelectWordsAndComments.ShowDialog() == DialogResult.OK)
            {
                tbWordsAndComments.Text = ofSelectWordsAndComments.FileName;
            }
        }

        private void cbWordsAndCommentsFile_CheckedChanged(object sender, EventArgs e)
        {
            tbWordsAndComments.Enabled = cbWordsAndCommentsFile.Checked;
        }

        private void cbManualInputWordAndComment_CheckedChanged(object sender, EventArgs e)
        {
            tbWord.Enabled = tbComment.Enabled = cbManualInputWordAndComment.Checked;
        }
    }
}
