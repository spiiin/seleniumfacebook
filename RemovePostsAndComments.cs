﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFacebook
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Support.UI;

    class RemovePostsAndComments
    {

        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string[] urlToBlock, bool testMode, int pagesToScrollDown)
        {
            removePostsAndComments(groups, urlToBlock, testMode, pagesToScrollDown);
        }

        private bool removePostAndComments(string testGroup, string linkToRemove, int nPages, bool testMode)
        {
            if (!formMain.asyncSleep(3000)) { return false; }
            formMain.getLog().AppendText("Removing post request from group: " + testGroup + "\n");
            var driver = formMain.getDriver();
            driver.Navigate().GoToUrl(testGroup);

            for (int i = 0; i < nPages; i++)
            {
                formMain.getLog().AppendText(String.Format("Get posts from page {0}/{1}\n", i + 1, nPages));
                if (!formMain.asyncSleep(5000)) { return false; }
                JsHelpers.scrollToBottomPage(driver);
                JsHelpers.waitWhileDocumentLoaded(driver);
            }

            if (!formMain.asyncSleep(5000)) { return false; }

            var posts = driver.FindElements(By.XPath("//div[contains(@id,'mall_post_')]"));
            formMain.getLog().AppendText("Total " + posts.Count.ToString() + " posts\n");
            foreach (var post in posts)
            {
                //formMain.getLog().AppendText("Author href " + aHref + "\n");
                if (needRemovePost(post, linkToRemove))
                {
                    formMain.getLog().AppendText("Post found! Trying to remove...\n");
                    clickToAdminPost(post);
                    if (!formMain.asyncSleep(1500)) { return false; }
                    clickToRemovePost();
                    if (!testMode)
                    {
                        clickToConfirmButton();
                    }
                    formMain.getLog().AppendText("Remove post from " + linkToRemove + "\n");
                    if (!formMain.asyncSleep(5000)) { return false; }
                    return true;
                }

                //remove comments. return true if at least one comments removed
                if (checkComments(post, linkToRemove, testMode))
                {
                    if (!formMain.asyncSleep(5000)) { return false; }
                    return true;
                }
            }
            return false;
        }

        private void removePostsAndComments(FormMain.GroupRec[] groups, string[] urlToBlock, bool testMode, int pagesToScrollDown)
        {
            int urlsCount = urlToBlock.Length;
            for (int u = 0; u < urlsCount; u++)
            {
                string linkToRemove = urlToBlock[u];
                formMain.getLog().AppendText("Removing posts for user: " + linkToRemove + "\n");
                for (int i = 0; i < groups.Length; i++)
                {
                    if (!formMain.asyncSleep(0))
                    {
                        formMain.getLog().AppendText("Stopped by user\n");
                        return;
                    }
                    var group = groups[i];
                    formMain.getLog().AppendText(String.Format("Proccess group {0}/{1} - {2}\n", i + 1, groups.Length, group.fullName));
                    bool needToContinue = true;
                    while (needToContinue)
                    {
                        try
                        {
                            needToContinue = removePostAndComments(group.shortName, linkToRemove, pagesToScrollDown, testMode);
                            needToContinue = false; //no repeats
                            formMain.getLog().AppendText("Reload posts after removing...\n");
                        }
                        catch (Exception e)
                        {
                            formMain.getLog().AppendText("Error while trying to remove posts and comments for group " + groups[i] + " .Message:" + e.Message + "\n");
                            needToContinue = false;
                        }
                    }
                }
                formMain.getLog().AppendText("Removing posts complete!\n");
            }
        }

        private bool clickToAdminPost(IWebElement post)
        {
            var adminButton = post.FindElement(By.XPath(".//a[contains(@data-testid,'post_chevron_button')]"));
            adminButton.Click();
            return true;
        }

        private bool clickToRemovePost()
        {
            if (!formMain.asyncSleep(500)) { return false; }
            var adminLinks = formMain.getDriver().FindElements(By.XPath("//a[@class='_54nc']"));
            foreach (var adminLink in adminLinks)
            {
                var linkHref = adminLink.GetAttribute("ajaxify");
                if (linkHref.Contains("delete.php"))
                {
                    adminLink.Click();
                    return true;
                }
            }
            return false;
        }

        private bool needRemovePost(IWebElement post, string linkToRemove)
        {
            var h5 = post.FindElement(By.XPath(".//h5"));
            var aLinks = h5.FindElements(By.XPath(".//a"));
            foreach (var aLink in aLinks)
            {
                var aHref = aLink.GetAttribute("href");
                bool linkIsUrl = CommonUtils.isUrl(linkToRemove);
                bool needRemove = linkIsUrl ? aHref.Contains(linkToRemove) : aLink.Text.Contains(linkToRemove);
                if (needRemove)
                {
                    return true;
                }
            }
            return false;
        }

        private bool clickToCommentAdminButton(IWebElement commentBlock)
        {
            var adminButton = commentBlock.FindElement(By.XPath(".//a[contains(@class,'UFICommentCloseButton')]"));
            adminButton.Click();
            return true;
        }

        private bool clickToCommentRemoveButton()
        {
            if (!formMain.asyncSleep(1000)) { return false; }
            var adminLinks = formMain.getDriver().FindElements(By.XPath("//a[contains(@class,'_54nc')]"));
            foreach (var adminLink in adminLinks)
            {
                var attr = adminLink.GetAttribute("data-testid");
                if (attr.Contains("ufi_comment_menu_delete"))
                {
                    adminLink.Click();
                    return true;
                }
            }
            return false;
        }

        private bool clickToRemoveCommentConfirmButton()
        {
            if (!formMain.asyncSleep(2000)) { return false; }
            var confirmButton = formMain.getDriver().FindElement(By.XPath("//a[contains(@data-testid,'ufi_hide_dialog_delete_button')]"));
            confirmButton.Click();
            return true;
        }

        private bool checkComments(IWebElement post, string linkToRemove, bool testMode)
        {
            var commentBlocks = post.FindElements(By.XPath(".//div[contains(@class,'UFICommentContentBlock')]"));
            if (commentBlocks.Count > 0)
            {
                formMain.getLog().AppendText("Found total comments: " + commentBlocks.Count + "\n");
            }
            foreach (var commentBlock in commentBlocks)
            {
                var aAuthor = commentBlock.FindElement(By.XPath(".//a[contains(@class,'UFICommentActorName')]"));
                var authorHref = aAuthor.GetAttribute("href");
                bool needRemove = CommonUtils.isUrl(linkToRemove) ? authorHref.Contains(linkToRemove) : aAuthor.Text.Contains(linkToRemove);
                if (needRemove)
                {
                    formMain.getLog().AppendText("Found comment from user. Trying to remove...\n");
                    clickToCommentAdminButton(commentBlock);
                    clickToCommentRemoveButton();
                    if (!testMode)
                    {
                        clickToRemoveCommentConfirmButton();
                    }
                    formMain.getLog().AppendText("Comment removed!\n");
                    return true;
                }
            }
            return false;
        }

        private bool clickToConfirmButton()
        {
            if (!this.formMain.asyncSleep(2000)) { return false; }
            var confirmButton = formMain.getDriver().FindElement(By.XPath("//button[contains(@class,'layerConfirm')]"));
            confirmButton.Click();
            return true;
        }
    }
}
