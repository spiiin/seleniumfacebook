﻿namespace SeleniumFacebook
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbUrlToGroup = new System.Windows.Forms.Label();
            this.tbUrlToGroupSimple = new System.Windows.Forms.TextBox();
            this.ofSelectFile = new System.Windows.Forms.OpenFileDialog();
            this.lbCredentials = new System.Windows.Forms.Label();
            this.tbCredentialsSimple = new System.Windows.Forms.TextBox();
            this.ofSelectCredentials = new System.Windows.Forms.OpenFileDialog();
            this.postButtonSimple = new System.Windows.Forms.Button();
            this.lbPostText = new System.Windows.Forms.Label();
            this.lbPostFileAttachment = new System.Windows.Forms.Label();
            this.tbPostTextSimple = new System.Windows.Forms.TextBox();
            this.tbPostAttachmentSimple = new System.Windows.Forms.TextBox();
            this.ofSelectJumperPosts = new System.Windows.Forms.OpenFileDialog();
            this.lbSleepBetweenPosts = new System.Windows.Forms.Label();
            this.tbPostSleepSimple = new System.Windows.Forms.TextBox();
            this.lbFileWithPostsUrls = new System.Windows.Forms.Label();
            this.tbJumperPostsSimple = new System.Windows.Forms.TextBox();
            this.ofSelectAttachment = new System.Windows.Forms.OpenFileDialog();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.cbBlockUser = new System.Windows.Forms.CheckBox();
            this.cbTestMode = new System.Windows.Forms.CheckBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btClearLog = new System.Windows.Forms.Button();
            this.cbRemoveFromGroups = new System.Windows.Forms.CheckBox();
            this.cbRun = new System.Windows.Forms.Button();
            this.cbRemoveMemberRequests = new System.Windows.Forms.CheckBox();
            this.cbRemovePostsAndComments = new System.Windows.Forms.CheckBox();
            this.cbUserInGroups = new System.Windows.Forms.CheckBox();
            this.cbRemingMembers = new System.Windows.Forms.CheckBox();
            this.cbPagesToScrollDown = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDeletePosts7 = new System.Windows.Forms.CheckBox();
            this.cbDeleteComments7 = new System.Windows.Forms.CheckBox();
            this.cbDeleteInvites7 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.oneGroupOnly = new System.Windows.Forms.CheckBox();
            this.oneGroupTextBox = new System.Windows.Forms.TextBox();
            this.btPostJumpers = new System.Windows.Forms.Button();
            this.submitQuestionButton = new System.Windows.Forms.Button();
            this.submitCommentButton = new System.Windows.Forms.Button();
            this.tbUrlToBlock = new System.Windows.Forms.TextBox();
            this.lbUrlToBlock = new System.Windows.Forms.Label();
            this.tbJumperComment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.newQuestionTextBox = new System.Windows.Forms.TextBox();
            this.newQuestion = new System.Windows.Forms.Label();
            this.tagInputTextBox = new System.Windows.Forms.TextBox();
            this.lblGroupTag = new System.Windows.Forms.Label();
            this.panelFull = new System.Windows.Forms.Panel();
            this.tbWord = new System.Windows.Forms.TextBox();
            this.tbComment = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbManualInputWordAndComment = new System.Windows.Forms.CheckBox();
            this.cbWordsAndCommentsFile = new System.Windows.Forms.CheckBox();
            this.tbWordsAndComments = new System.Windows.Forms.TextBox();
            this.checkBoxGroupFull = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.checkedListBoxGroupsFull = new System.Windows.Forms.CheckedListBox();
            this.linkLabelReloadFull = new System.Windows.Forms.LinkLabel();
            this.postButtonFull = new System.Windows.Forms.Button();
            this.tbJumperPostsFull = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPostSleepFull = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPostAttachmentFull = new System.Windows.Forms.TextBox();
            this.tbPostTextFull = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbUrlToGroupFull = new System.Windows.Forms.TextBox();
            this.tbCredentialsFull = new System.Windows.Forms.TextBox();
            this.linkLabelReloadSimple = new System.Windows.Forms.LinkLabel();
            this.checkedListBoxGroupsSimple = new System.Windows.Forms.CheckedListBox();
            this.lbGroupsToManipulate = new System.Windows.Forms.Label();
            this.panelSimple = new System.Windows.Forms.Panel();
            this.checkBoxGroupSimple = new System.Windows.Forms.CheckBox();
            this.ofSelectWordsAndComments = new System.Windows.Forms.OpenFileDialog();
            this.panelFull.SuspendLayout();
            this.panelSimple.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbUrlToGroup
            // 
            this.lbUrlToGroup.AutoSize = true;
            this.lbUrlToGroup.Location = new System.Drawing.Point(14, 39);
            this.lbUrlToGroup.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbUrlToGroup.Name = "lbUrlToGroup";
            this.lbUrlToGroup.Size = new System.Drawing.Size(80, 12);
            this.lbUrlToGroup.TabIndex = 3;
            this.lbUrlToGroup.Text = "File with groups";
            // 
            // tbUrlToGroupSimple
            // 
            this.tbUrlToGroupSimple.Location = new System.Drawing.Point(127, 36);
            this.tbUrlToGroupSimple.Margin = new System.Windows.Forms.Padding(2);
            this.tbUrlToGroupSimple.Name = "tbUrlToGroupSimple";
            this.tbUrlToGroupSimple.Size = new System.Drawing.Size(277, 22);
            this.tbUrlToGroupSimple.TabIndex = 4;
            this.tbUrlToGroupSimple.Text = "Groups.txt";
            this.tbUrlToGroupSimple.Click += new System.EventHandler(this.tbUrlToGroup_Click);
            // 
            // ofSelectFile
            // 
            this.ofSelectFile.FileName = "Groups.txt";
            this.ofSelectFile.InitialDirectory = ".";
            // 
            // lbCredentials
            // 
            this.lbCredentials.AutoSize = true;
            this.lbCredentials.Location = new System.Drawing.Point(13, 11);
            this.lbCredentials.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCredentials.Name = "lbCredentials";
            this.lbCredentials.Size = new System.Drawing.Size(97, 12);
            this.lbCredentials.TabIndex = 12;
            this.lbCredentials.Text = "File with credentials";
            // 
            // tbCredentialsSimple
            // 
            this.tbCredentialsSimple.Enabled = false;
            this.tbCredentialsSimple.Location = new System.Drawing.Point(127, 7);
            this.tbCredentialsSimple.Margin = new System.Windows.Forms.Padding(2);
            this.tbCredentialsSimple.Name = "tbCredentialsSimple";
            this.tbCredentialsSimple.Size = new System.Drawing.Size(260, 22);
            this.tbCredentialsSimple.TabIndex = 13;
            this.tbCredentialsSimple.Text = "Credentials.txt";
            this.tbCredentialsSimple.Click += new System.EventHandler(this.tbCredentials_Click);
            // 
            // ofSelectCredentials
            // 
            this.ofSelectCredentials.FileName = "Groups.txt";
            this.ofSelectCredentials.InitialDirectory = ".";
            // 
            // postButtonSimple
            // 
            this.postButtonSimple.Location = new System.Drawing.Point(127, 490);
            this.postButtonSimple.Margin = new System.Windows.Forms.Padding(2);
            this.postButtonSimple.Name = "postButtonSimple";
            this.postButtonSimple.Size = new System.Drawing.Size(187, 50);
            this.postButtonSimple.TabIndex = 45;
            this.postButtonSimple.Text = "Post to groups";
            this.postButtonSimple.UseVisualStyleBackColor = true;
            this.postButtonSimple.Click += new System.EventHandler(this.postButton_Click);
            // 
            // lbPostText
            // 
            this.lbPostText.AutoSize = true;
            this.lbPostText.Location = new System.Drawing.Point(16, 342);
            this.lbPostText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbPostText.Name = "lbPostText";
            this.lbPostText.Size = new System.Drawing.Size(44, 12);
            this.lbPostText.TabIndex = 46;
            this.lbPostText.Text = "Post text";
            // 
            // lbPostFileAttachment
            // 
            this.lbPostFileAttachment.AutoSize = true;
            this.lbPostFileAttachment.Location = new System.Drawing.Point(14, 415);
            this.lbPostFileAttachment.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbPostFileAttachment.Name = "lbPostFileAttachment";
            this.lbPostFileAttachment.Size = new System.Drawing.Size(95, 12);
            this.lbPostFileAttachment.TabIndex = 47;
            this.lbPostFileAttachment.Text = "Post file attachment";
            // 
            // tbPostTextSimple
            // 
            this.tbPostTextSimple.Location = new System.Drawing.Point(127, 339);
            this.tbPostTextSimple.Margin = new System.Windows.Forms.Padding(2);
            this.tbPostTextSimple.Multiline = true;
            this.tbPostTextSimple.Name = "tbPostTextSimple";
            this.tbPostTextSimple.Size = new System.Drawing.Size(277, 69);
            this.tbPostTextSimple.TabIndex = 48;
            // 
            // tbPostAttachmentSimple
            // 
            this.tbPostAttachmentSimple.Location = new System.Drawing.Point(127, 412);
            this.tbPostAttachmentSimple.Margin = new System.Windows.Forms.Padding(2);
            this.tbPostAttachmentSimple.Name = "tbPostAttachmentSimple";
            this.tbPostAttachmentSimple.Size = new System.Drawing.Size(277, 22);
            this.tbPostAttachmentSimple.TabIndex = 49;
            this.tbPostAttachmentSimple.Click += new System.EventHandler(this.tbPostAttachment_Click);
            // 
            // ofSelectJumperPosts
            // 
            this.ofSelectJumperPosts.FileName = "Posts.txt";
            // 
            // lbSleepBetweenPosts
            // 
            this.lbSleepBetweenPosts.AutoSize = true;
            this.lbSleepBetweenPosts.Location = new System.Drawing.Point(14, 441);
            this.lbSleepBetweenPosts.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbSleepBetweenPosts.Name = "lbSleepBetweenPosts";
            this.lbSleepBetweenPosts.Size = new System.Drawing.Size(97, 12);
            this.lbSleepBetweenPosts.TabIndex = 50;
            this.lbSleepBetweenPosts.Text = "Sleep between posts";
            // 
            // tbPostSleepSimple
            // 
            this.tbPostSleepSimple.Location = new System.Drawing.Point(127, 438);
            this.tbPostSleepSimple.Margin = new System.Windows.Forms.Padding(2);
            this.tbPostSleepSimple.Name = "tbPostSleepSimple";
            this.tbPostSleepSimple.Size = new System.Drawing.Size(277, 22);
            this.tbPostSleepSimple.TabIndex = 51;
            this.tbPostSleepSimple.Text = "600";
            // 
            // lbFileWithPostsUrls
            // 
            this.lbFileWithPostsUrls.AutoSize = true;
            this.lbFileWithPostsUrls.Location = new System.Drawing.Point(14, 467);
            this.lbFileWithPostsUrls.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbFileWithPostsUrls.Name = "lbFileWithPostsUrls";
            this.lbFileWithPostsUrls.Size = new System.Drawing.Size(91, 12);
            this.lbFileWithPostsUrls.TabIndex = 53;
            this.lbFileWithPostsUrls.Text = "File with posts urls";
            // 
            // tbJumperPostsSimple
            // 
            this.tbJumperPostsSimple.Location = new System.Drawing.Point(127, 464);
            this.tbJumperPostsSimple.Margin = new System.Windows.Forms.Padding(2);
            this.tbJumperPostsSimple.Name = "tbJumperPostsSimple";
            this.tbJumperPostsSimple.Size = new System.Drawing.Size(277, 22);
            this.tbJumperPostsSimple.TabIndex = 54;
            this.tbJumperPostsSimple.Text = "Posts.txt";
            this.tbJumperPostsSimple.Click += new System.EventHandler(this.tbJumperPosts_Click);
            // 
            // ofSelectAttachment
            // 
            this.ofSelectAttachment.FileName = "attachment.png";
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(9, 633);
            this.tbLog.Margin = new System.Windows.Forms.Padding(2);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(388, 75);
            this.tbLog.TabIndex = 0;
            // 
            // cbBlockUser
            // 
            this.cbBlockUser.AutoSize = true;
            this.cbBlockUser.Location = new System.Drawing.Point(9, 567);
            this.cbBlockUser.Margin = new System.Windows.Forms.Padding(2);
            this.cbBlockUser.Name = "cbBlockUser";
            this.cbBlockUser.Size = new System.Drawing.Size(331, 16);
            this.cbBlockUser.TabIndex = 10;
            this.cbBlockUser.Text = "Also block user (for removing from groups and member requests)";
            this.cbBlockUser.UseVisualStyleBackColor = true;
            // 
            // cbTestMode
            // 
            this.cbTestMode.AutoSize = true;
            this.cbTestMode.Location = new System.Drawing.Point(9, 585);
            this.cbTestMode.Margin = new System.Windows.Forms.Padding(2);
            this.cbTestMode.Name = "cbTestMode";
            this.cbTestMode.Size = new System.Drawing.Size(365, 16);
            this.cbTestMode.TabIndex = 11;
            this.cbTestMode.Text = "Test mode (only find user in posts, members, etc. No real removing/bans)";
            this.cbTestMode.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(118, 608);
            this.btnStop.Margin = new System.Windows.Forms.Padding(2);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(105, 21);
            this.btnStop.TabIndex = 14;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btClearLog
            // 
            this.btClearLog.Location = new System.Drawing.Point(227, 608);
            this.btClearLog.Margin = new System.Windows.Forms.Padding(2);
            this.btClearLog.Name = "btClearLog";
            this.btClearLog.Size = new System.Drawing.Size(105, 22);
            this.btClearLog.TabIndex = 16;
            this.btClearLog.Text = "Clear log";
            this.btClearLog.UseVisualStyleBackColor = true;
            this.btClearLog.Click += new System.EventHandler(this.btClearLog_Click);
            // 
            // cbRemoveFromGroups
            // 
            this.cbRemoveFromGroups.AutoSize = true;
            this.cbRemoveFromGroups.Location = new System.Drawing.Point(9, 445);
            this.cbRemoveFromGroups.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemoveFromGroups.Name = "cbRemoveFromGroups";
            this.cbRemoveFromGroups.Size = new System.Drawing.Size(174, 16);
            this.cbRemoveFromGroups.TabIndex = 17;
            this.cbRemoveFromGroups.Text = "Remove from groups and delete";
            this.cbRemoveFromGroups.UseVisualStyleBackColor = true;
            // 
            // cbRun
            // 
            this.cbRun.Location = new System.Drawing.Point(9, 608);
            this.cbRun.Margin = new System.Windows.Forms.Padding(2);
            this.cbRun.Name = "cbRun";
            this.cbRun.Size = new System.Drawing.Size(105, 21);
            this.cbRun.TabIndex = 18;
            this.cbRun.Text = "Run";
            this.cbRun.UseVisualStyleBackColor = true;
            this.cbRun.Click += new System.EventHandler(this.cbRun_Click);
            // 
            // cbRemoveMemberRequests
            // 
            this.cbRemoveMemberRequests.AutoSize = true;
            this.cbRemoveMemberRequests.Location = new System.Drawing.Point(9, 465);
            this.cbRemoveMemberRequests.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemoveMemberRequests.Name = "cbRemoveMemberRequests";
            this.cbRemoveMemberRequests.Size = new System.Drawing.Size(144, 16);
            this.cbRemoveMemberRequests.TabIndex = 19;
            this.cbRemoveMemberRequests.Text = "Remove member requests";
            this.cbRemoveMemberRequests.UseVisualStyleBackColor = true;
            // 
            // cbRemovePostsAndComments
            // 
            this.cbRemovePostsAndComments.AutoSize = true;
            this.cbRemovePostsAndComments.Location = new System.Drawing.Point(9, 484);
            this.cbRemovePostsAndComments.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemovePostsAndComments.Name = "cbRemovePostsAndComments";
            this.cbRemovePostsAndComments.Size = new System.Drawing.Size(159, 16);
            this.cbRemovePostsAndComments.TabIndex = 20;
            this.cbRemovePostsAndComments.Text = "Remove posts and comments";
            this.cbRemovePostsAndComments.UseVisualStyleBackColor = true;
            // 
            // cbUserInGroups
            // 
            this.cbUserInGroups.AutoSize = true;
            this.cbUserInGroups.Location = new System.Drawing.Point(9, 504);
            this.cbUserInGroups.Margin = new System.Windows.Forms.Padding(2);
            this.cbUserInGroups.Name = "cbUserInGroups";
            this.cbUserInGroups.Size = new System.Drawing.Size(133, 16);
            this.cbUserInGroups.TabIndex = 21;
            this.cbUserInGroups.Text = "Check if user in groups";
            this.cbUserInGroups.UseVisualStyleBackColor = true;
            // 
            // cbRemingMembers
            // 
            this.cbRemingMembers.AutoSize = true;
            this.cbRemingMembers.Location = new System.Drawing.Point(9, 522);
            this.cbRemingMembers.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemingMembers.Name = "cbRemingMembers";
            this.cbRemingMembers.Size = new System.Drawing.Size(199, 16);
            this.cbRemingMembers.TabIndex = 22;
            this.cbRemingMembers.Text = "Remind members to answer questions";
            this.cbRemingMembers.UseVisualStyleBackColor = true;
            // 
            // cbPagesToScrollDown
            // 
            this.cbPagesToScrollDown.FormattingEnabled = true;
            this.cbPagesToScrollDown.Location = new System.Drawing.Point(119, 542);
            this.cbPagesToScrollDown.Margin = new System.Windows.Forms.Padding(2);
            this.cbPagesToScrollDown.Name = "cbPagesToScrollDown";
            this.cbPagesToScrollDown.Size = new System.Drawing.Size(92, 20);
            this.cbPagesToScrollDown.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 545);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 12);
            this.label1.TabIndex = 24;
            this.label1.Text = "Pages to scroll down";
            // 
            // cbDeletePosts7
            // 
            this.cbDeletePosts7.AutoSize = true;
            this.cbDeletePosts7.Checked = true;
            this.cbDeletePosts7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDeletePosts7.Location = new System.Drawing.Point(188, 445);
            this.cbDeletePosts7.Margin = new System.Windows.Forms.Padding(2);
            this.cbDeletePosts7.Name = "cbDeletePosts7";
            this.cbDeletePosts7.Size = new System.Drawing.Size(47, 16);
            this.cbDeletePosts7.TabIndex = 25;
            this.cbDeletePosts7.Text = "posts";
            this.cbDeletePosts7.UseVisualStyleBackColor = true;
            // 
            // cbDeleteComments7
            // 
            this.cbDeleteComments7.AutoSize = true;
            this.cbDeleteComments7.Checked = true;
            this.cbDeleteComments7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDeleteComments7.Location = new System.Drawing.Point(239, 445);
            this.cbDeleteComments7.Margin = new System.Windows.Forms.Padding(2);
            this.cbDeleteComments7.Name = "cbDeleteComments7";
            this.cbDeleteComments7.Size = new System.Drawing.Size(71, 16);
            this.cbDeleteComments7.TabIndex = 26;
            this.cbDeleteComments7.Text = "comments";
            this.cbDeleteComments7.UseVisualStyleBackColor = true;
            // 
            // cbDeleteInvites7
            // 
            this.cbDeleteInvites7.AutoSize = true;
            this.cbDeleteInvites7.Location = new System.Drawing.Point(181, 465);
            this.cbDeleteInvites7.Margin = new System.Windows.Forms.Padding(2);
            this.cbDeleteInvites7.Name = "cbDeleteInvites7";
            this.cbDeleteInvites7.Size = new System.Drawing.Size(54, 16);
            this.cbDeleteInvites7.TabIndex = 27;
            this.cbDeleteInvites7.Text = "invites";
            this.cbDeleteInvites7.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 466);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 12);
            this.label2.TabIndex = 28;
            this.label2.Text = "for last 7 days (if available)";
            // 
            // oneGroupOnly
            // 
            this.oneGroupOnly.AutoSize = true;
            this.oneGroupOnly.Location = new System.Drawing.Point(9, 712);
            this.oneGroupOnly.Margin = new System.Windows.Forms.Padding(2);
            this.oneGroupOnly.Name = "oneGroupOnly";
            this.oneGroupOnly.Size = new System.Drawing.Size(102, 16);
            this.oneGroupOnly.TabIndex = 37;
            this.oneGroupOnly.Text = "This group only:";
            this.oneGroupOnly.UseVisualStyleBackColor = true;
            this.oneGroupOnly.Visible = false;
            this.oneGroupOnly.CheckedChanged += new System.EventHandler(this.oneGroupOnly_CheckedChanged);
            // 
            // oneGroupTextBox
            // 
            this.oneGroupTextBox.Location = new System.Drawing.Point(129, 712);
            this.oneGroupTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.oneGroupTextBox.Name = "oneGroupTextBox";
            this.oneGroupTextBox.Size = new System.Drawing.Size(268, 22);
            this.oneGroupTextBox.TabIndex = 38;
            this.oneGroupTextBox.Visible = false;
            this.oneGroupTextBox.TextChanged += new System.EventHandler(this.oneGroupTextBox_TextChanged);
            // 
            // btPostJumpers
            // 
            this.btPostJumpers.Location = new System.Drawing.Point(303, 421);
            this.btPostJumpers.Margin = new System.Windows.Forms.Padding(2);
            this.btPostJumpers.Name = "btPostJumpers";
            this.btPostJumpers.Size = new System.Drawing.Size(95, 21);
            this.btPostJumpers.TabIndex = 52;
            this.btPostJumpers.Text = "Post jumpers";
            this.btPostJumpers.UseVisualStyleBackColor = true;
            this.btPostJumpers.Click += new System.EventHandler(this.btPostJumpers_Click);
            // 
            // submitQuestionButton
            // 
            this.submitQuestionButton.Location = new System.Drawing.Point(106, 421);
            this.submitQuestionButton.Margin = new System.Windows.Forms.Padding(2);
            this.submitQuestionButton.Name = "submitQuestionButton";
            this.submitQuestionButton.Size = new System.Drawing.Size(96, 21);
            this.submitQuestionButton.TabIndex = 36;
            this.submitQuestionButton.Text = "Submit question";
            this.submitQuestionButton.UseVisualStyleBackColor = true;
            this.submitQuestionButton.Click += new System.EventHandler(this.submitCommentQuestion_Click);
            // 
            // submitCommentButton
            // 
            this.submitCommentButton.Location = new System.Drawing.Point(9, 421);
            this.submitCommentButton.Margin = new System.Windows.Forms.Padding(2);
            this.submitCommentButton.Name = "submitCommentButton";
            this.submitCommentButton.Size = new System.Drawing.Size(95, 21);
            this.submitCommentButton.TabIndex = 33;
            this.submitCommentButton.Text = "Submit comment";
            this.submitCommentButton.UseVisualStyleBackColor = true;
            this.submitCommentButton.Click += new System.EventHandler(this.submitCommentButton_Click);
            // 
            // tbUrlToBlock
            // 
            this.tbUrlToBlock.Location = new System.Drawing.Point(9, 330);
            this.tbUrlToBlock.Margin = new System.Windows.Forms.Padding(2);
            this.tbUrlToBlock.Multiline = true;
            this.tbUrlToBlock.Name = "tbUrlToBlock";
            this.tbUrlToBlock.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbUrlToBlock.Size = new System.Drawing.Size(388, 87);
            this.tbUrlToBlock.TabIndex = 1;
            // 
            // lbUrlToBlock
            // 
            this.lbUrlToBlock.AutoSize = true;
            this.lbUrlToBlock.Location = new System.Drawing.Point(7, 316);
            this.lbUrlToBlock.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbUrlToBlock.Name = "lbUrlToBlock";
            this.lbUrlToBlock.Size = new System.Drawing.Size(188, 12);
            this.lbUrlToBlock.TabIndex = 2;
            this.lbUrlToBlock.Text = "URL to profiles to block (one per line):";
            // 
            // tbJumperComment
            // 
            this.tbJumperComment.Location = new System.Drawing.Point(123, 291);
            this.tbJumperComment.Margin = new System.Windows.Forms.Padding(2);
            this.tbJumperComment.Name = "tbJumperComment";
            this.tbJumperComment.Size = new System.Drawing.Size(274, 22);
            this.tbJumperComment.TabIndex = 56;
            this.tbJumperComment.Text = "###jumper";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 294);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 12);
            this.label7.TabIndex = 55;
            this.label7.Text = "Jumper comment";
            // 
            // newQuestionTextBox
            // 
            this.newQuestionTextBox.Location = new System.Drawing.Point(123, 134);
            this.newQuestionTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.newQuestionTextBox.Name = "newQuestionTextBox";
            this.newQuestionTextBox.Size = new System.Drawing.Size(274, 22);
            this.newQuestionTextBox.TabIndex = 35;
            // 
            // newQuestion
            // 
            this.newQuestion.AutoSize = true;
            this.newQuestion.Location = new System.Drawing.Point(7, 137);
            this.newQuestion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.newQuestion.Name = "newQuestion";
            this.newQuestion.Size = new System.Drawing.Size(71, 12);
            this.newQuestion.TabIndex = 34;
            this.newQuestion.Text = "New question:";
            // 
            // tagInputTextBox
            // 
            this.tagInputTextBox.Location = new System.Drawing.Point(123, 112);
            this.tagInputTextBox.Name = "tagInputTextBox";
            this.tagInputTextBox.Size = new System.Drawing.Size(274, 22);
            this.tagInputTextBox.TabIndex = 44;
            // 
            // lblGroupTag
            // 
            this.lblGroupTag.AutoSize = true;
            this.lblGroupTag.Location = new System.Drawing.Point(7, 115);
            this.lblGroupTag.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGroupTag.Name = "lblGroupTag";
            this.lblGroupTag.Size = new System.Drawing.Size(56, 12);
            this.lblGroupTag.TabIndex = 43;
            this.lblGroupTag.Text = "Group-tag:";
            // 
            // panelFull
            // 
            this.panelFull.Controls.Add(this.tbWord);
            this.panelFull.Controls.Add(this.tbComment);
            this.panelFull.Controls.Add(this.label11);
            this.panelFull.Controls.Add(this.cbManualInputWordAndComment);
            this.panelFull.Controls.Add(this.cbWordsAndCommentsFile);
            this.panelFull.Controls.Add(this.tbWordsAndComments);
            this.panelFull.Controls.Add(this.checkBoxGroupFull);
            this.panelFull.Controls.Add(this.label10);
            this.panelFull.Controls.Add(this.checkedListBoxGroupsFull);
            this.panelFull.Controls.Add(this.linkLabelReloadFull);
            this.panelFull.Controls.Add(this.postButtonFull);
            this.panelFull.Controls.Add(this.tbJumperPostsFull);
            this.panelFull.Controls.Add(this.label5);
            this.panelFull.Controls.Add(this.tbPostSleepFull);
            this.panelFull.Controls.Add(this.label6);
            this.panelFull.Controls.Add(this.tbPostAttachmentFull);
            this.panelFull.Controls.Add(this.tbPostTextFull);
            this.panelFull.Controls.Add(this.label8);
            this.panelFull.Controls.Add(this.label9);
            this.panelFull.Controls.Add(this.label3);
            this.panelFull.Controls.Add(this.label4);
            this.panelFull.Controls.Add(this.tbUrlToGroupFull);
            this.panelFull.Controls.Add(this.tbCredentialsFull);
            this.panelFull.Controls.Add(this.tbLog);
            this.panelFull.Controls.Add(this.tbUrlToBlock);
            this.panelFull.Controls.Add(this.tbJumperComment);
            this.panelFull.Controls.Add(this.lbUrlToBlock);
            this.panelFull.Controls.Add(this.label7);
            this.panelFull.Controls.Add(this.cbBlockUser);
            this.panelFull.Controls.Add(this.cbTestMode);
            this.panelFull.Controls.Add(this.btnStop);
            this.panelFull.Controls.Add(this.btPostJumpers);
            this.panelFull.Controls.Add(this.btClearLog);
            this.panelFull.Controls.Add(this.cbRemoveFromGroups);
            this.panelFull.Controls.Add(this.cbRun);
            this.panelFull.Controls.Add(this.cbRemoveMemberRequests);
            this.panelFull.Controls.Add(this.cbRemovePostsAndComments);
            this.panelFull.Controls.Add(this.cbUserInGroups);
            this.panelFull.Controls.Add(this.cbRemingMembers);
            this.panelFull.Controls.Add(this.cbPagesToScrollDown);
            this.panelFull.Controls.Add(this.tagInputTextBox);
            this.panelFull.Controls.Add(this.label1);
            this.panelFull.Controls.Add(this.lblGroupTag);
            this.panelFull.Controls.Add(this.cbDeletePosts7);
            this.panelFull.Controls.Add(this.oneGroupTextBox);
            this.panelFull.Controls.Add(this.cbDeleteComments7);
            this.panelFull.Controls.Add(this.oneGroupOnly);
            this.panelFull.Controls.Add(this.cbDeleteInvites7);
            this.panelFull.Controls.Add(this.submitQuestionButton);
            this.panelFull.Controls.Add(this.label2);
            this.panelFull.Controls.Add(this.newQuestionTextBox);
            this.panelFull.Controls.Add(this.newQuestion);
            this.panelFull.Controls.Add(this.submitCommentButton);
            this.panelFull.Location = new System.Drawing.Point(12, 12);
            this.panelFull.Name = "panelFull";
            this.panelFull.Size = new System.Drawing.Size(671, 748);
            this.panelFull.TabIndex = 59;
            // 
            // tbWord
            // 
            this.tbWord.Enabled = false;
            this.tbWord.Location = new System.Drawing.Point(181, 68);
            this.tbWord.Name = "tbWord";
            this.tbWord.Size = new System.Drawing.Size(216, 22);
            this.tbWord.TabIndex = 80;
            // 
            // tbComment
            // 
            this.tbComment.Enabled = false;
            this.tbComment.Location = new System.Drawing.Point(181, 90);
            this.tbComment.Name = "tbComment";
            this.tbComment.Size = new System.Drawing.Size(216, 22);
            this.tbComment.TabIndex = 79;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 12);
            this.label11.TabIndex = 78;
            this.label11.Text = "Comment";
            // 
            // cbManualInputWordAndComment
            // 
            this.cbManualInputWordAndComment.AutoSize = true;
            this.cbManualInputWordAndComment.Location = new System.Drawing.Point(9, 74);
            this.cbManualInputWordAndComment.Name = "cbManualInputWordAndComment";
            this.cbManualInputWordAndComment.Size = new System.Drawing.Size(123, 16);
            this.cbManualInputWordAndComment.TabIndex = 77;
            this.cbManualInputWordAndComment.Text = "Word to find in posts";
            this.cbManualInputWordAndComment.UseVisualStyleBackColor = true;
            this.cbManualInputWordAndComment.CheckedChanged += new System.EventHandler(this.cbManualInputWordAndComment_CheckedChanged);
            // 
            // cbWordsAndCommentsFile
            // 
            this.cbWordsAndCommentsFile.AutoSize = true;
            this.cbWordsAndCommentsFile.Checked = true;
            this.cbWordsAndCommentsFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWordsAndCommentsFile.Location = new System.Drawing.Point(9, 49);
            this.cbWordsAndCommentsFile.Name = "cbWordsAndCommentsFile";
            this.cbWordsAndCommentsFile.Size = new System.Drawing.Size(165, 16);
            this.cbWordsAndCommentsFile.TabIndex = 76;
            this.cbWordsAndCommentsFile.Text = "File with words and comments";
            this.cbWordsAndCommentsFile.UseVisualStyleBackColor = true;
            this.cbWordsAndCommentsFile.CheckedChanged += new System.EventHandler(this.cbWordsAndCommentsFile_CheckedChanged);
            // 
            // tbWordsAndComments
            // 
            this.tbWordsAndComments.Location = new System.Drawing.Point(181, 46);
            this.tbWordsAndComments.Name = "tbWordsAndComments";
            this.tbWordsAndComments.Size = new System.Drawing.Size(216, 22);
            this.tbWordsAndComments.TabIndex = 75;
            this.tbWordsAndComments.Text = "WordsAndComments.txt";
            this.tbWordsAndComments.Click += new System.EventHandler(this.tbWordsAndComments_Click);
            // 
            // checkBoxGroupFull
            // 
            this.checkBoxGroupFull.AutoSize = true;
            this.checkBoxGroupFull.Location = new System.Drawing.Point(600, 5);
            this.checkBoxGroupFull.Name = "checkBoxGroupFull";
            this.checkBoxGroupFull.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGroupFull.TabIndex = 73;
            this.checkBoxGroupFull.UseVisualStyleBackColor = true;
            this.checkBoxGroupFull.CheckedChanged += new System.EventHandler(this.checkBoxGroup_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(403, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 12);
            this.label10.TabIndex = 72;
            this.label10.Text = "Groups to manipulate";
            // 
            // checkedListBoxGroupsFull
            // 
            this.checkedListBoxGroupsFull.CheckOnClick = true;
            this.checkedListBoxGroupsFull.FormattingEnabled = true;
            this.checkedListBoxGroupsFull.Location = new System.Drawing.Point(403, 20);
            this.checkedListBoxGroupsFull.MultiColumn = true;
            this.checkedListBoxGroupsFull.Name = "checkedListBoxGroupsFull";
            this.checkedListBoxGroupsFull.Size = new System.Drawing.Size(259, 718);
            this.checkedListBoxGroupsFull.TabIndex = 71;
            // 
            // linkLabelReloadFull
            // 
            this.linkLabelReloadFull.AutoSize = true;
            this.linkLabelReloadFull.Location = new System.Drawing.Point(621, 5);
            this.linkLabelReloadFull.Name = "linkLabelReloadFull";
            this.linkLabelReloadFull.Size = new System.Drawing.Size(38, 12);
            this.linkLabelReloadFull.TabIndex = 70;
            this.linkLabelReloadFull.TabStop = true;
            this.linkLabelReloadFull.Text = "Reload";
            this.linkLabelReloadFull.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelReload_LinkClicked);
            // 
            // postButtonFull
            // 
            this.postButtonFull.Location = new System.Drawing.Point(205, 421);
            this.postButtonFull.Margin = new System.Windows.Forms.Padding(2);
            this.postButtonFull.Name = "postButtonFull";
            this.postButtonFull.Size = new System.Drawing.Size(95, 21);
            this.postButtonFull.TabIndex = 69;
            this.postButtonFull.Text = "Post to groups";
            this.postButtonFull.UseVisualStyleBackColor = true;
            this.postButtonFull.Click += new System.EventHandler(this.postButton_Click);
            // 
            // tbJumperPostsFull
            // 
            this.tbJumperPostsFull.Location = new System.Drawing.Point(123, 269);
            this.tbJumperPostsFull.Margin = new System.Windows.Forms.Padding(2);
            this.tbJumperPostsFull.Name = "tbJumperPostsFull";
            this.tbJumperPostsFull.Size = new System.Drawing.Size(274, 22);
            this.tbJumperPostsFull.TabIndex = 68;
            this.tbJumperPostsFull.Text = "Posts.txt";
            this.tbJumperPostsFull.Click += new System.EventHandler(this.tbJumperPosts_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 272);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 12);
            this.label5.TabIndex = 67;
            this.label5.Text = "File with posts urls";
            // 
            // tbPostSleepFull
            // 
            this.tbPostSleepFull.Location = new System.Drawing.Point(123, 247);
            this.tbPostSleepFull.Margin = new System.Windows.Forms.Padding(2);
            this.tbPostSleepFull.Name = "tbPostSleepFull";
            this.tbPostSleepFull.Size = new System.Drawing.Size(274, 22);
            this.tbPostSleepFull.TabIndex = 66;
            this.tbPostSleepFull.Text = "600";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 250);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 12);
            this.label6.TabIndex = 65;
            this.label6.Text = "Sleep between posts";
            // 
            // tbPostAttachmentFull
            // 
            this.tbPostAttachmentFull.Location = new System.Drawing.Point(123, 225);
            this.tbPostAttachmentFull.Margin = new System.Windows.Forms.Padding(2);
            this.tbPostAttachmentFull.Name = "tbPostAttachmentFull";
            this.tbPostAttachmentFull.Size = new System.Drawing.Size(274, 22);
            this.tbPostAttachmentFull.TabIndex = 64;
            this.tbPostAttachmentFull.Click += new System.EventHandler(this.tbPostAttachment_Click);
            // 
            // tbPostTextFull
            // 
            this.tbPostTextFull.Location = new System.Drawing.Point(123, 156);
            this.tbPostTextFull.Margin = new System.Windows.Forms.Padding(2);
            this.tbPostTextFull.Multiline = true;
            this.tbPostTextFull.Name = "tbPostTextFull";
            this.tbPostTextFull.Size = new System.Drawing.Size(274, 69);
            this.tbPostTextFull.TabIndex = 63;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 159);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 12);
            this.label8.TabIndex = 61;
            this.label8.Text = "Post text";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 228);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 12);
            this.label9.TabIndex = 62;
            this.label9.Text = "Post file attachment";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 12);
            this.label3.TabIndex = 59;
            this.label3.Text = "File with credentials";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 12);
            this.label4.TabIndex = 57;
            this.label4.Text = "File with groups";
            // 
            // tbUrlToGroupFull
            // 
            this.tbUrlToGroupFull.Location = new System.Drawing.Point(123, 24);
            this.tbUrlToGroupFull.Margin = new System.Windows.Forms.Padding(2);
            this.tbUrlToGroupFull.Name = "tbUrlToGroupFull";
            this.tbUrlToGroupFull.Size = new System.Drawing.Size(274, 22);
            this.tbUrlToGroupFull.TabIndex = 58;
            this.tbUrlToGroupFull.Text = "Groups.txt";
            this.tbUrlToGroupFull.Click += new System.EventHandler(this.tbUrlToGroup_Click);
            // 
            // tbCredentialsFull
            // 
            this.tbCredentialsFull.Enabled = false;
            this.tbCredentialsFull.Location = new System.Drawing.Point(123, 2);
            this.tbCredentialsFull.Margin = new System.Windows.Forms.Padding(2);
            this.tbCredentialsFull.Name = "tbCredentialsFull";
            this.tbCredentialsFull.Size = new System.Drawing.Size(274, 22);
            this.tbCredentialsFull.TabIndex = 60;
            this.tbCredentialsFull.Text = "Credentials.txt";
            this.tbCredentialsFull.Click += new System.EventHandler(this.tbCredentials_Click);
            // 
            // linkLabelReloadSimple
            // 
            this.linkLabelReloadSimple.AutoSize = true;
            this.linkLabelReloadSimple.Location = new System.Drawing.Point(80, 93);
            this.linkLabelReloadSimple.Name = "linkLabelReloadSimple";
            this.linkLabelReloadSimple.Size = new System.Drawing.Size(38, 12);
            this.linkLabelReloadSimple.TabIndex = 60;
            this.linkLabelReloadSimple.TabStop = true;
            this.linkLabelReloadSimple.Text = "Reload";
            this.linkLabelReloadSimple.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelReload_LinkClicked);
            // 
            // checkedListBoxGroupsSimple
            // 
            this.checkedListBoxGroupsSimple.CheckOnClick = true;
            this.checkedListBoxGroupsSimple.FormattingEnabled = true;
            this.checkedListBoxGroupsSimple.Location = new System.Drawing.Point(127, 64);
            this.checkedListBoxGroupsSimple.MultiColumn = true;
            this.checkedListBoxGroupsSimple.Name = "checkedListBoxGroupsSimple";
            this.checkedListBoxGroupsSimple.Size = new System.Drawing.Size(277, 276);
            this.checkedListBoxGroupsSimple.TabIndex = 61;
            // 
            // lbGroupsToManipulate
            // 
            this.lbGroupsToManipulate.AutoSize = true;
            this.lbGroupsToManipulate.Location = new System.Drawing.Point(16, 70);
            this.lbGroupsToManipulate.Name = "lbGroupsToManipulate";
            this.lbGroupsToManipulate.Size = new System.Drawing.Size(105, 12);
            this.lbGroupsToManipulate.TabIndex = 62;
            this.lbGroupsToManipulate.Text = "Groups to manipulate";
            // 
            // panelSimple
            // 
            this.panelSimple.Controls.Add(this.checkBoxGroupSimple);
            this.panelSimple.Controls.Add(this.lbCredentials);
            this.panelSimple.Controls.Add(this.lbGroupsToManipulate);
            this.panelSimple.Controls.Add(this.tbJumperPostsSimple);
            this.panelSimple.Controls.Add(this.lbUrlToGroup);
            this.panelSimple.Controls.Add(this.lbFileWithPostsUrls);
            this.panelSimple.Controls.Add(this.checkedListBoxGroupsSimple);
            this.panelSimple.Controls.Add(this.tbPostSleepSimple);
            this.panelSimple.Controls.Add(this.tbUrlToGroupSimple);
            this.panelSimple.Controls.Add(this.lbSleepBetweenPosts);
            this.panelSimple.Controls.Add(this.linkLabelReloadSimple);
            this.panelSimple.Controls.Add(this.tbPostAttachmentSimple);
            this.panelSimple.Controls.Add(this.tbCredentialsSimple);
            this.panelSimple.Controls.Add(this.tbPostTextSimple);
            this.panelSimple.Controls.Add(this.lbPostText);
            this.panelSimple.Controls.Add(this.lbPostFileAttachment);
            this.panelSimple.Controls.Add(this.postButtonSimple);
            this.panelSimple.Location = new System.Drawing.Point(499, 5);
            this.panelSimple.Name = "panelSimple";
            this.panelSimple.Size = new System.Drawing.Size(412, 548);
            this.panelSimple.TabIndex = 63;
            this.panelSimple.Visible = false;
            // 
            // checkBoxGroupSimple
            // 
            this.checkBoxGroupSimple.AutoSize = true;
            this.checkBoxGroupSimple.Location = new System.Drawing.Point(100, 113);
            this.checkBoxGroupSimple.Name = "checkBoxGroupSimple";
            this.checkBoxGroupSimple.Size = new System.Drawing.Size(15, 14);
            this.checkBoxGroupSimple.TabIndex = 63;
            this.checkBoxGroupSimple.UseVisualStyleBackColor = true;
            this.checkBoxGroupSimple.CheckedChanged += new System.EventHandler(this.checkBoxGroup_CheckedChanged);
            // 
            // ofSelectWordsAndComments
            // 
            this.ofSelectWordsAndComments.FileName = "WordsAndComments.txt";
            this.ofSelectWordsAndComments.InitialDirectory = ".";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 731);
            this.Controls.Add(this.panelSimple);
            this.Controls.Add(this.panelFull);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(650, 770);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Facebook Selenium 2.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            this.panelFull.ResumeLayout(false);
            this.panelFull.PerformLayout();
            this.panelSimple.ResumeLayout(false);
            this.panelSimple.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbUrlToGroup;
        private System.Windows.Forms.TextBox tbUrlToGroupSimple;
        private System.Windows.Forms.OpenFileDialog ofSelectFile;
        private System.Windows.Forms.Label lbCredentials;
        private System.Windows.Forms.TextBox tbCredentialsSimple;
        private System.Windows.Forms.OpenFileDialog ofSelectCredentials;
        private System.Windows.Forms.Button postButtonSimple;
        private System.Windows.Forms.Label lbPostText;
        private System.Windows.Forms.Label lbPostFileAttachment;
        private System.Windows.Forms.TextBox tbPostTextSimple;
        private System.Windows.Forms.TextBox tbPostAttachmentSimple;
        private System.Windows.Forms.OpenFileDialog ofSelectJumperPosts;
        private System.Windows.Forms.Label lbSleepBetweenPosts;
        private System.Windows.Forms.TextBox tbPostSleepSimple;
        private System.Windows.Forms.Label lbFileWithPostsUrls;
        private System.Windows.Forms.TextBox tbJumperPostsSimple;
        private System.Windows.Forms.OpenFileDialog ofSelectAttachment;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.CheckBox cbBlockUser;
        private System.Windows.Forms.CheckBox cbTestMode;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btClearLog;
        private System.Windows.Forms.CheckBox cbRemoveFromGroups;
        private System.Windows.Forms.Button cbRun;
        private System.Windows.Forms.CheckBox cbRemoveMemberRequests;
        private System.Windows.Forms.CheckBox cbRemovePostsAndComments;
        private System.Windows.Forms.CheckBox cbUserInGroups;
        private System.Windows.Forms.CheckBox cbRemingMembers;
        private System.Windows.Forms.ComboBox cbPagesToScrollDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbDeletePosts7;
        private System.Windows.Forms.CheckBox cbDeleteComments7;
        private System.Windows.Forms.CheckBox cbDeleteInvites7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox oneGroupOnly;
        private System.Windows.Forms.TextBox oneGroupTextBox;
        private System.Windows.Forms.Button btPostJumpers;
        private System.Windows.Forms.Button submitQuestionButton;
        private System.Windows.Forms.Button submitCommentButton;
        private System.Windows.Forms.TextBox tbUrlToBlock;
        private System.Windows.Forms.Label lbUrlToBlock;
        private System.Windows.Forms.TextBox tbJumperComment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox newQuestionTextBox;
        private System.Windows.Forms.Label newQuestion;
        private System.Windows.Forms.TextBox tagInputTextBox;
        private System.Windows.Forms.Label lblGroupTag;
        private System.Windows.Forms.Panel panelFull;
        private System.Windows.Forms.LinkLabel linkLabelReloadSimple;
        private System.Windows.Forms.CheckedListBox checkedListBoxGroupsSimple;
        private System.Windows.Forms.Label lbGroupsToManipulate;
        private System.Windows.Forms.Panel panelSimple;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbUrlToGroupFull;
        private System.Windows.Forms.TextBox tbCredentialsFull;
        private System.Windows.Forms.Button postButtonFull;
        private System.Windows.Forms.TextBox tbJumperPostsFull;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPostSleepFull;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPostAttachmentFull;
        private System.Windows.Forms.TextBox tbPostTextFull;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckedListBox checkedListBoxGroupsFull;
        private System.Windows.Forms.LinkLabel linkLabelReloadFull;
        private System.Windows.Forms.CheckBox checkBoxGroupSimple;
        private System.Windows.Forms.CheckBox checkBoxGroupFull;
        private System.Windows.Forms.TextBox tbWordsAndComments;
        private System.Windows.Forms.OpenFileDialog ofSelectWordsAndComments;
        private System.Windows.Forms.CheckBox cbWordsAndCommentsFile;
        private System.Windows.Forms.TextBox tbWord;
        private System.Windows.Forms.TextBox tbComment;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox cbManualInputWordAndComment;
    }
}

