Installation:
Software uses WebDriver/Selenium library and Firefox browser, so your need to install Firefox before running application.

You need to specify:
* File with credentials - txt file with your login in first line and facebook in second line.
* File with groups - txt file with groups. One group per line.
* URL to profile to block - link to page that you want to block.

Notes on work:
* All stages logged in software and can be viewed in browser.
* After start application and before run any tasks, click to "Login to Facebook" button - you must be login to perform actions.
* Removing from groups - at first, tool go to page with user, that you want to block, and grab real name, so it can be found in search at group member list. It's much faster than check all users in group (it can be thousands of pages with all users).
* "Also block user" checkbox works with "Remove from groups" and "Remove member requess" actions. It will ban user from group permanently.
* Removing posts and comments. There is only one way to get more posts in group - scroll to bottom of the page and wait while posts loaded. Tool check 5 pages back.
* After removing post or comment by banned person tool must be reload page and repeat search again from beginning. It seems, there is a bug in Selenium library, and after rebuilding page DOM, posts data becomes invalid, so it need to refresh page to continue search, so tasks is not very fast.
* Test mode supported for any actions - if it activated, just search of data will be executed, not real removing (for example - user will be found in list, and ban button will be clicked, but confirm button will not be clicked). So you can freely experiment with real users without risk of accidentally ban it.
* The bot behaves like a real user, so it has pauses between actions to wait while data loaded and to avoid to get ban from Facebook. I set big pauses (2-5 seconds) between actions for sure. Pauses can be slightly reduced, but it takes long time to calculate the exact limits (and after ban there are many captchas from facebook, I think). If you want more speed - you can run 2-3 copy of application with different profiles to block.

