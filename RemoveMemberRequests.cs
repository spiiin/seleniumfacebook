﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    class RemoveMemberRequests
    {
        public FormMain formMain { get; set; }

        public void start(FormMain.GroupRec[] groups, string[] urlsToBlock, bool blockUser, bool testMode)
        {
            removeMemberRequests(groups, urlsToBlock, blockUser, testMode);
        }

        private void removeMemberRequest(string testGroup, string linkToRemove, bool blockUser, bool testMode)
        {
            if (!formMain.asyncSleep(3000)) { return; }
            formMain.getLog().AppendText("Removing member request from group: " + testGroup + "\n");
            if (!testGroup.EndsWith("/"))
            {
                testGroup = testGroup + "/";
            }
            testGroup = testGroup + "requests";
            var driver = formMain.getDriver();
            driver.Navigate().GoToUrl(testGroup);

            //
            if (!formMain.asyncSleep(5000)) { return; }

            var listWithRequests = driver.FindElement(By.XPath("//ul[@class='uiList _4kg _6-h _6-j _4ks' or @class='uiList _4kg  _4kt _6-h _6-j']")); //get list with requests - several types of classes
            var requestItems = listWithRequests.FindElements(By.XPath("./*"));                                //select all items in list
            formMain.getLog().AppendText("Total " + requestItems.Count.ToString() + " requests\n");                       //print request count
            foreach (var requestItem in requestItems)
            {
                var aDiv = requestItem.FindElement(By.XPath(".//div[@class='_2rcs fsl fwb fcb' or @class='fsm fwn fcg']"));
                //insertCheckboxToPage(aDiv);

                var aRequest = aDiv.FindElement(By.TagName("a"));
                var requestHref = aRequest.GetAttribute("href");
                //formMain.getLog().AppendText("Request href " + requestHref + "\n");
                bool linkIsUrl = CommonUtils.isUrl(linkToRemove);
                bool needRemoveRequest = linkIsUrl ? requestHref.Contains(linkToRemove) : aRequest.Text.Contains(linkToRemove);
                if (needRemoveRequest)
                {
                    formMain.getLog().AppendText("Request found! Trying to remove...\n");
                    //old interface
                    if (clickToRemoveRequest(requestItem, blockUser, testMode))
                    {
                        formMain.getLog().AppendText("Remove request from " + linkToRemove + "\n");
                        return;
                    }
                    //new interface
                    else if (clickToRemoveButton(requestItem, blockUser, testMode))
                    {
                        formMain.getLog().AppendText("Remove request from " + linkToRemove + "\n");
                        return;
                    }
                       
                }
            }
        }

        private void removeMemberRequests(FormMain.GroupRec[] groups, string[] urlsToBlock, bool blockUser, bool testMode)
        {
            int urlsCount = urlsToBlock.Length;
            for (int u = 0; u < urlsCount; u++)
            {
                string linkToRemove = urlsToBlock[u];
                formMain.getLog().AppendText("Removing member requests begin. User: " + linkToRemove + "\n");

                for (int i = 0; i < groups.Length; i++)
                {
                    if (!formMain.asyncSleep(0))
                    {
                        formMain.getLog().AppendText("Stopped by user\n");
                        return;
                    }
                    try
                    {
                        var group = groups[i];
                        formMain.getLog().AppendText(String.Format("Proccess group {0}/{1} - {2}\n", i + 1, groups.Length, group.fullName));
                        removeMemberRequest(group.shortName, linkToRemove, blockUser, testMode);
                    }
                    catch (Exception e)
                    {
                        formMain.getLog().AppendText("Error while trying to remove member requests from group " + groups[i].fullName + ". Message:" + e.Message + "\n");
                    }
                }
                formMain.getLog().AppendText("Removing member requests complete!\n");
            }
        }

        private bool clickToRemoveButton(IWebElement requestItem, bool blockUser, bool testMode)
        {
            try
            {
                if (testMode)
                {
                    return true;
                }

                var buttons = requestItem.FindElements(By.XPath(".//button[@type='submit']"));

                if (blockUser)
                {

                    buttons[2].Click(); //third button is "block" button.
                    return true;
                }
                else
                {
                    buttons[1].Click(); //second button is "decline" button
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private bool clickToRemoveRequest(IWebElement requestItem, bool blockUser, bool testMode)
        {
            try
            {
                //var aRemove = requestItem.FindElement(By.XPath(".//a[@name='approve button']"));
                if (testMode)
                {
                    return true;
                }

                if (blockUser)
                {
                    var aRemove = requestItem.FindElement(By.XPath(".//a[@name='block button']"));
                    aRemove.Click();
                    return true;
                }
                else
                {
                    var aRemove = requestItem.FindElement(By.XPath(".//a[@name='decline button']"));
                    aRemove.Click();
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
