﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    class FindFullName
    {
        public FormMain formMain { get; set; }

        public string findFullName(string profileUrl, out string newLinkToRemove)
        {
            newLinkToRemove = profileUrl;
            var driver = formMain.getDriver();
            driver.Navigate().GoToUrl(profileUrl);
            if (!formMain.asyncSleep(2000)) { return ""; }
            newLinkToRemove = driver.Url;
            var fullnameSpan = new WebDriverWait(driver, new TimeSpan(0, 0, 10)).Until(ExpectedConditions.ElementExists(By.Id("fb-timeline-cover-name")));
            return JsHelpers.getElementTextWithoutChildText(driver, fullnameSpan);
        }
    }
}
