﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumFacebook
{
    static class JsHelpers
    {
        public static string getElementTextWithoutChildText(IWebDriver driver, IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            return (string)js.ExecuteScript(@"
            var parent = arguments[0];
            var child = parent.firstChild;
            var ret = '';
            while (child)
            {
                if (child.nodeType === Node.TEXT_NODE)
                    ret += child.textContent;
                child = child.nextSibling;
            }
            return ret;
            ", element);
        }

        public static void waitWhileDocumentLoaded(IWebDriver _driver)
        {
            new WebDriverWait(_driver, new TimeSpan(0, 0, 30)).Until((driver) =>
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                return (string)js.ExecuteScript(@"
                    return document.readyState;
                    ") == "complete";
            });
        }

        public static void scrollToBottomPage(IWebDriver driver)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0,document.body.scrollHeight);");
        }

        public static void insertCheckboxToPage(IWebDriver driver, IWebElement element)
        {
            var jsCode = @"
            var parent = arguments[0];
            var checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.name = 'name';
            checkbox.value = 'value';
            checkbox.id = 'id';

            parent.appendChild(checkbox);";

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript(jsCode, element);
        }
    }
}
